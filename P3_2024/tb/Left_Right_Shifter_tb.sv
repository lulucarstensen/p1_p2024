

`timescale 1ns / 1ps // Adjust the timescale as needed

module Left_Right_Shifter_tb;


	// Parameters
	localparam DW = 10, shift = 1, ext_b = 0;

    // Inputs
	 logic clk;
    logic right_or_left;
    logic [DW - 1: 0]data_to_shift;
	 
	 // Output
    logic [DW - 1: 0]shifted_o;
	 
	 // Instantiate the module 
	 
	 shift_right_left uut
	 (
		.clk(clk),
		.right_or_left(right_or_left),
		.data_to_shift(data_to_shift),
		
		
		.shifted_o(shifted_o)
	 
	 );
	 
	  // Stimulus
    initial begin
		right_or_left = 0;
        data_to_shift = 10'b1010101010;
        #2;

        // Test right shift without extending bits

        #2 right_or_left = 0;
        #2 right_or_left = 1;
        #2;

        // Test right shift with extending bits
        right_or_left = 0;
        data_to_shift = 10'b1010101010;
        #2 right_or_left = 0;
        #2 right_or_left = 1;
        #2;

        // Test left shift
        right_or_left = 1;
        data_to_shift = 10'b1010101010;
        #2 right_or_left = 0;
        #2 right_or_left = 1;
        #2;

	 
	 end
	 
	  // Generate clock signal
always begin
    #1 clk = ~clk;
end

endmodule 