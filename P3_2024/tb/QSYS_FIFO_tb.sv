

`timescale 1ns / 1ps

module QSYS_FIFO_tb;

localparam CLK_SYNC = 1'b0;
localparam DW = 8;
localparam AW = 8;
localparam AB = 3;


// Module inputs
logic clk_rd;
logic clk_wr;
logic rst;
logic rd_i;
logic wr_i;
logic [DW-1:0] data_i;

// Module outputs
logic full;
logic empty;
logic [DW-1:0] data_o;

// Instantiate the FIFO module
FIFO #(
    .CLK_SYNC(CLK_SYNC), // Choose which clock is faster, rd or wr
    .DW(DW),
    .AW(AW),
    .AB(AB)
) fifo_inst (
    .clk_rd(clk_rd),
    .clk_wr(clk_wr),
    .rst(rst),
    .rd_i(rd_i),
    .wr_i(wr_i),
    .data_i(data_i),
    .full(full),
    .empty(empty),
    .data_o(data_o)
);

// Generate clock signals
always begin
    #0.5 clk_rd = ~clk_rd;
end

always begin
    #0.5 clk_wr = ~clk_wr;
end

// Stimulus generation
initial begin
	 clk_rd = 0;
	 clk_wr = 0;
    rst = 1'b0;
    rd_i = 1'b0;
    wr_i = 1'b1;
    data_i = 8'b00000000;

    #1 rst = 1'b1; // De-assert reset 
	 #1 rst = 1'b1; // De-assert reset 

    // Write operations
    #2 wr_i = 1'b1; 
	 data_i = 8'b00000001; // Write data 001
    #0.75 wr_i = 1'b0;
	 
    #3 wr_i = 1'b1; 
	 #0.25 data_i = 8'b00000010; // Write data 010
    #0.75 wr_i = 1'b0;
	 
    #4 wr_i = 1'b1; 
	 #0.25 data_i = 8'b00000011; // Write data 011
    #0.75 wr_i = 1'b0;
	 
	 #5 wr_i = 1'b1; 
	 #0.25 data_i = 8'b00000100; // Write data 100
    #0.75 wr_i = 1'b0;

	 #6 wr_i = 1'b1; 
	 #0.25 data_i = 8'b00000101; // Write data 101
    #0.75 wr_i = 1'b0;
	 
	 #7 wr_i = 1'b1; 
	 #0.25 data_i = 8'b00000111; // Write data 111
    #0.75 wr_i = 1'b0;
	 
	 #8 wr_i = 1'b1; 
	 #0.25 data_i = 8'b00001000; // Write data 1000
    #0.75 wr_i = 1'b0;	 
	 
	 #9 wr_i = 1'b1; 
	 #0.25 data_i = 8'b00001001; // Write data 1001
    #0.75 wr_i = 1'b0;	 	 
	 
    // Read operations
    #10 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;
    
	 #11 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;
    
	 #12 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;
	 
    #13 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;
    
	 #14 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;
    
	 #15 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;	 

    #16 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;
    
	 #17 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;
    
	 #18 rd_i = 1'b1; // Read data
    #1 rd_i = 1'b0;

	 
    // Add more test scenarios as needed
end

endmodule 