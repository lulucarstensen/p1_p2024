`timescale 1ns / 1ps // Adjust the timescale as needed

module RR_submodule_tb;

    // Parameters
    localparam DW = 16;  // Data Width
    localparam AB = 4;  // Address Bits

	 
    // Signals
    logic clk;
    logic rst;
    logic enb;
    logic [DW - 1:0] request;
    logic [AB - 1:0] decoded_data;

    // Instantiate the DUT
    Arbiter_Submodule #(

        .DW(DW),
		   .AB(AB)
    ) uut (
        .clk(clk),
        .rst(rst),
        .enb(enb),
        .data_request(request),

        .decoded_data(decoded_data)
    );

// Generate clock signal
always begin
    #1 clk = ~clk;
end

    // Reset generation
    initial begin
		  clk = 0;
        rst = 0;
        enb = 0;
        #10 rst = 1;
   

    // Stimulus

        enb = 1;
        request = 16'b0000000000000100; // Requestor 1 has the highest priority
        #10;
//        request = 8'b00000010; // Requestor 2 has the highest priority
//        #10;
//        request = 8'b00001000; // Requestor 3 has the highest priority
//        #10;
//        request = 8'b00000100; // Requestor 4 has the highest priority
        #100;
    end

endmodule
