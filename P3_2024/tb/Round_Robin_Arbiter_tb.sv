module Round_Robin_Arbiter_TB;

    // Parameters
    localparam NR = 8;  // Number of Requestors
    localparam NW = 8;  // Number of Writers
    localparam DW = 8;  // Data Width
    localparam ADDR_W = 8;  // Address Width
    localparam ADDR_B = 4;  // Address Bits

    // Signals
    logic clk;
    logic rst;
    logic enb;
    logic [NR - 1:0] vec_request;
    logic [NR - 1:0][DW + ADDR_W - 1:0] FIFO_data_i;
    logic [DW - 1:0] FIFO_data_o;
    logic [NR - 1:0] read;
    logic [ADDR_B - 1:0] decoded_data;
    logic write;

    // Instantiate the DUT
    Round_Robin_Arbiter #(
        .NR(NR),
        .NW(NW),
        .DW(DW),
        .ADDR_W(ADDR_W),
        .ADDR_B(ADDR_B)
    ) uut (
        .clk(clk),
        .rst(rst),
        .enb(enb),
        .vec_request(vec_request),
        .FIFO_data_i(FIFO_data_i),
        .FIFO_data_o(FIFO_data_o),
        .read(read),
        .decoded_data(decoded_data),
        .write(write)
    );

    // Clock generation
always begin
    #1 clk <= ~clk;
end

    // Reset generation
    initial begin
clk = 0;
        rst = 0;
        enb = 0;
        #2 rst = 1;
    end

    // Stimulus
    initial begin
        #2;
        enb = 1;
        vec_request = 8'b00000001; // Requestor 1 has the highest priority
        FIFO_data_i[0] = 8'b00001101; // Data for requestor 0
        FIFO_data_i[1] = 8'b00000010; // Data for requestor 1
        FIFO_data_i[2] = 8'b00001111; // Data for requestor 2
        FIFO_data_i[3] = 8'b00000101; // Data for requestor 3
		  FIFO_data_i[4] = 8'b00001101; // Data for requestor 0
        FIFO_data_i[5] = 8'b00000010; // Data for requestor 1
        FIFO_data_i[6] = 8'b00001111; // Data for requestor 2
        FIFO_data_i[7] = 8'b00000101; // Data for requestor 3
        #10;
        vec_request = 8'b00000010; // Requestor 2 has the highest priority
        #10;
        vec_request = 8'b00001000; // Requestor 3 has the highest priority
        #10;
        vec_request = 8'b00000100; // Requestor 4 has the highest priority
#10;
        vec_request = 8'b00000100; // Requestor 4 has the highest priority
        #100;
        $finish;
    end

endmodule
