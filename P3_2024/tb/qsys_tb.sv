`timescale 1ns / 1ps

module QSYS_tb;

localparam DW = 8;
localparam AB_DD = 4;
localparam MW = 4;
localparam NW = 4;
localparam AB_M = 4;
localparam AB_N = 4;
localparam DATA = 10;

logic clk;
logic rst;
logic enb;
logic [MW-1:0]fifo_input_write;
logic [MW-1:0][DW+AB_DD-1:0]FIFO_i_data_i;
logic [NW-1:0]read_fifo_out_l;
logic [NW-1:0]empty_fifo_out_l;
logic [NW-1:0][DW-1:0]fifo_output_data;

logic [AB_M:0]i;
logic [AB_M:0]j; 

logic ready_to_tx;
logic [AB_N-1:0] address_l;

QSYS
#(
	.DW(DW),
	.DD(DATA), //RAM SIZE
	.MW(MW),
	.NW(NW),
	.AB_DD(AB_DD),
	.AB_M(AB_M), //log2(MW)+1,
	.AB_N(AB_N) //log2(NW)+1
) QSYS
(
   .clk_50Mhz(clk),
   .clk_10Mhz(),
   .rst(rst),
   .enb(enb),
   .fifo_input_write(fifo_input_write),
   .fifo_input_data(FIFO_i_data_i),
   .read_fifo_out(read_fifo_out_l),
   .empty_fifo_out(empty_fifo_out_l),
   .fifo_output_data(fifo_output_data)	
);

clear_output_fifos #(
   .FIFOS(MW),
   .FB(AB_M)
) clear_output_f
(
   .enb(enb),
   .clk(clk),
   .rst(rst),
   .request_fifos(empty_fifo_out_l),
   .available_TX(ready_to_tx),
   .address(address_l),
   .read_fifo(read_fifo_out_l),
   .send_data()
);

initial begin
	  clk            = 1'b0;
	  rst            = 1'b0;
      fifo_input_write  = 1'b0;
      enb            = 1'b0;
      FIFO_i_data_i  = 12'b0000_0000_0000;
#3    rst            = 1'b1;


$display("FOR");
for(i = 0 ; i < DATA ; i++)
begin: fill_fifo
#2 $display("Inside for");
   for(j = 0; j < MW ; j++)
   begin
       FIFO_i_data_i[j] = {(4'b0+($urandom%(NW))),(8'b0+j)};
       //$display("urandom: %0d" ,FIFO_i_data_i[j])
       $display("j: %0d" ,j);
   end
   $display("i: %0d",i);
   fifo_input_write = {MW{1'b1}};
end: fill_fifo
#2 fifo_input_write = {MW{1'b0}};

#2   enb             = 1'b1;
#6   ready_to_tx	 = 1'b1;
#6  ready_to_tx	 = 1'b0;
#6   ready_to_tx	 = 1'b1;
#6  ready_to_tx	 = 1'b0;
#6   ready_to_tx	 = 1'b1;
#6  ready_to_tx	 = 1'b0;
// #2    read_fifo_out_l = 4'b0001;
// #2    read_fifo_out_l = 4'b0001;
// #6    read_fifo_out_l = 4'b0001;
// #2    read_fifo_out_l = 4'b0010;
// #2    read_fifo_out_l = 4'b0100;
// #2    read_fifo_out_l = 4'b1000;
#10000
    $stop;

end

always begin
    #1 clk <= ~clk;
end

endmodule

