//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		Round Robin Interface
//Date:	  		May 8th, 2024


`ifndef Interface_RR
	`define Interface_RR


	interface Interface_RR
#(
		parameter DW = 8,
		parameter AW = 4, 
		parameter MW = 8,
		parameter NW = 8
);

logic [MW-1:0][DW+AW-1:0]input_data;
logic [NW-1:0][DW-1:0]output_data;
logic [NW-1:0][MW-1:0]request;
logic [MW-1:0][NW-1:0]read;
logic [NW-1:0]write;


modport RR
(
   input request,      
   input input_data,  
	
   output read,
   output output_data, 
   output write
);

modport input_FIFO
(
   input read,
	
   output request,
   output input_data
);


modport output_FIFO
(
   input write,
   input output_data
);

endinterface
`endif