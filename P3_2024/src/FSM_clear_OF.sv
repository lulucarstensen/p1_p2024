//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		FIFO State Machine
//Date:	  		May 8th, 2024

import data_pkg::*;

module FSM_clear_OF
(  
    input clk,
    input rst,
    input enb,
    input available_TX,
    output send_data,
    output read_fifo
);

CLEAR_OF_state current_state;
CLEAR_OF_state next_state;

logic send_l;
logic read_fifo_l;

always_comb begin
    case(current_state)
    IDLE_CO:begin
        next_state = (available_TX == 1'b1) ? TRANSMIT : IDLE_CO;
    end
    TRANSMIT:begin
        next_state = (available_TX == 1'b0) ? READ : TRANSMIT;
    end
    READ:begin
        next_state = IDLE_CO;
    end
    default: begin
        next_state = IDLE_CO;
    end
    endcase
end

always_ff@(posedge clk or negedge rst)
begin
    if(rst == 1'b0)
        current_state <= IDLE_CO;
    else if(enb == 1'b1)
        current_state <= next_state;
end

always_comb begin
    case(current_state)
		IDLE_CO: begin
		   send_l = 1'b0;
		   read_fifo_l = 1'b0;
		end    
		TRANSMIT: begin
		   send_l = 1'b1;
		   read_fifo_l = 1'b0;
		end        
		READ: begin
		   send_l = 1'b0;
		   read_fifo_l = 1'b1;
		end      
		default: begin
		   send_l = 1'b0;
		   read_fifo_l = 1'b0;
		end
    endcase
end

assign send_data = send_l;
assign read_fifo = read_fifo_l;

endmodule
