//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		FIFO
//Date:	  		May 8th, 2024

module FIFO  
	#( 
		parameter CLK_SYNC = 1'b0, // 0 -> wr faster, 1 -> rd faster
		parameter DW = 8,
		parameter AW = 8,// Data Depth, Address Width
		parameter AB = 3// Address Bits, not smaller than log2(AW)
	)
(
	input clk_rd,
	input clk_wr,
	input rst,
	input rd_i,
	input wr_i,
	input [DW - 1: 0]data_i,
	
	output full,
	output empty,
	output [DW - 1: 0]data_o
);

logic [DW-1:0]data_l;

/*		W I R E S	*/
wire [AB-1:0]read_addr_w;
wire [AB-1:0]write_addr_w;
wire we_sel_w;
wire we_null_w;
wire rd_sel_w;
wire read_null_w;
wire read_sync_rst_w;
wire write_sync_rst_w;
wire last_read_w;
wire last_write_w;

ram_dual #(
	.DW(DW),
	.AW(AW),
	.AB(AB)
)					ram_dp
(
	.data(data_i),
	.read_addr(read_addr_w), 
	.write_addr(write_addr_w),
	.we(we_null_w),
	.clk(clk_wr),
	
	.q(data_l)
);

// MUX WRITE WE
mux_2_1 #(
	.DW(1)
)					mux_write_we
(
	.sel(we_sel_w),
   .dataA(wr_i),
   .dataB(0),
	
	.dataOut(we_null_w) // Write Enable or Zero
);

// MUX READ 
mux_2_1 #(
	.DW(1)
)					mux_read_we
(
	.sel(rd_sel_w),
   .dataA(rd_i),
   .dataB(0),
	
	.dataOut(read_null_w) // Read or Zero
);
	
// COUNTER WRITE ADDRESS
counter #(
	.DW(AB) // Instance to Address Bits
)					counter_Write_Addr
(
    .enb(we_null_w),
    .clk(clk_wr),
    .rst(rst),
    .sync_rst(write_sync_rst_w),
	 
    .count_o(write_addr_w)
);

// COUNTER READ ADDRESS
counter #(
	.DW(AB) // Instance to Address Bits
)					counter_Read_Addr
(
	 .enb(read_null_w),
    .clk(clk_rd),
    .rst(rst),
    .sync_rst(read_sync_rst_w),
	 
    .count_o(read_addr_w)
);

// SISO WRITE
siso_register
#(
	.N(1)
)				siso_write
(
    .clk(clk_wr),
    .reset(rst),
    .enable(1),
	 .sync_rst(0),
    .DataIn(wr_i),
	 
	 .DataOut(last_write_w)
);

// SISO READ
siso_register
#(
	.N(1)
)				siso_read
(
    .clk(clk_rd),
    .reset(rst),
    .enable(1),
	 .sync_rst(0),
    .DataIn(rd_i),
	 
	 .DataOut(last_read_w)
);

// FSM FIFO
FSM_FIFO 
#(
	.ADDR_B(AB),
	.ADDR_W(AW)
)						fsm_fifo
(
		.clk(clk),
		.rst(rst),
		.enb(1), // always on 
		.wr(last_write_w), // push
		.rd(last_read_w), // pop
		.wr_addr(write_addr_w),
		.rd_addr(read_addr_w),

		.full(full),
		.empty(empty),
		.we(we_sel_w),
		.rd_o(rd_sel_w), // pop
		.wr_syncrst(write_sync_rst_w),
		.rd_syncrst (read_sync_rst_w)
);

// CLOCK ASSIGNMENT FOR FSM
assign clk = (CLK_SYNC == 1'b0) ? clk_rd : clk_wr;

assign data_o = data_l;

endmodule 