// Project: QSYS
// Module:  Data Pkg
// Author:  María Luisa Ortiz Carstensen and Victor Cervantes
// Date:    May 6th, 2024


package data_pkg;

// * Parameters * // 

//* ARBITER'S PARAMETERS *//

// 	* SHIFT :

localparam SHIFT		= 1'b1;
localparam EXT_BIT	= 1'b0; // ON -> 1'b1, OFF -> 1'b0;



// *	FIFO FSM *	//

localparam FSM1_S = 1;
localparam FSM2_S = 1;

typedef enum logic [FSM1_S : 0]
{
    IDLE     = 2'b00,
    EMPTY    = 2'b01,
    FULL     = 2'b10
}FIFO_state;

//* ----------------	*//



typedef enum logic [FSM2_S : 0]
{
    IDLE_CO  = 2'b00,
    TRANSMIT = 2'b01,
    READ     = 2'b10
}CLEAR_OF_state;
	

	
// *	FSM COMMANDS 	*//
localparam FSM3_S = 4;

typedef enum logic [FSM3_S - 1: 0] 
{
		IDLE_CMD 		= 4'b0000,
		LENGTH			= 4'b0001,
		COMMAND			= 4'b0010,
		PORT_SETTING	= 4'b0011,
		FILL_BEGIN		= 4'b0100,
		FILL				= 4'b0101,
		ARBITER_ENB		= 4'b0110,
		RX					= 4'b0111,
		EOF				= 4'b1000,
		ERROR				= 4'b1001,
		RX_IDLE			= 4'b1010
		
}cmd_state;	
	
endpackage	
