//Author:  		Victor Cervantes
//Project: 		QSYS
//Module:  		Clear output fifos
//Date:	  		May 8th, 2024 

module clear_output_fifos #(
   parameter FIFOS = 16,
   parameter FB = 4
)
(
   input enb,
   input clk,
   input rst,
   input [FIFOS-1:0] request_fifos,
   input available_TX,
   output [FB-1:0] address,
   output [FIFOS-1:0] read_fifo,
   output send_data
);

localparam SHIFT_LEFT = 1'b1;

wire [FIFOS-1:0] ppc_out_w;
wire [FIFOS-1:0] shifter_out_w;
wire read_trigger_w;
wire send_trigger_w;

logic [FIFOS-1:0] send_data_l;
logic [FB-1:0] address_l;

ppc
#(
	.MW(FIFOS)
) ppc_unit
(
	.data_input(request_fifos),
	.data_output(ppc_out_w)
);

shift_right_left #(
	.DW(FIFOS)
) shifter
(
	.right_or_left(SHIFT_LEFT),
	.data_to_shift(ppc_out_w),
	.shifted_o(shifter_out_w)
);

binary_decoder_DW
#(
	.DW(FIFOS),
	.AB(FB)
) binary_decoder
(
	.data_input(send_data_l),
	.data_output(address_l)
);

FSM_clear_OF 
fsm_clr_output_fifos
(  
   .clk(clk),
   .rst(rst),
   .enb(enb),
   .available_TX(available_TX),
   .send_data(send_trigger_w),
   .read_fifo(read_trigger_w)
);

assign send_data_l = ppc_out_w ^ shifter_out_w;
assign read_fifo = (enb == 1 && read_trigger_w == 1'b1) ? send_data_l : {FIFOS{1'b0}} ;
assign address = address_l;
assign send_data = (enb == 1) ? send_trigger_w : 1'b0;

endmodule
