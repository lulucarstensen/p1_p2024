//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		Left or Right Shifter 
//Date:	  		May 8th, 2024

import data_pkg::*;

module shift_right_left
#( 
	DW = 10
 )

(
	input right_or_left, //right = 0, left = 1
	input [DW - 1: 0] data_to_shift,
	
	output [DW - 1: 0] shifted_o	
);

logic [DW - 1: 0] data_to_shift_l; 
logic [DW - 1: 0] shifted_l;



always_comb begin
    if (!right_or_left) begin
        if (EXT_BIT == 1'b0)
            data_to_shift_l = data_to_shift >> SHIFT;
        else
            data_to_shift_l = {{SHIFT{data_to_shift[DW-1]}}, shifted_l[( DW-1 )- SHIFT : 0]};
    end
    else 
        data_to_shift_l = data_to_shift << SHIFT;
end

 
// Register to hold the shifted data temporarily during shifting operations
assign shifted_l = data_to_shift[( DW - 1 ): 0] >> (SHIFT); 

// Output Assignment
assign shifted_o = data_to_shift_l;

endmodule

