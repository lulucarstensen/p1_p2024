//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Counter
//Date:	  		13/02/2024

//
// This modules counts the clock cycles giving as an output the current count
//

module counter
#(
    parameter DW = 4
)
(
    input enb,
    input clk,
    input rst,
    input sync_rst,
    output [DW-1 : 0] count_o
);
logic [DW-1 : 0] Output_l;

always_ff@(posedge clk or negedge rst)
begin
    if(!rst)
        Output_l <= {(DW-1){1'b0}};
    else
		begin
			if(enb)
			begin 
				if(sync_rst)
					Output_l <= {(DW-1){1'b0}};
				else 
					begin
						Output_l <= Output_l + 1'b1;
					end
			end     
		end
end

assign	count_o = Output_l;

endmodule

