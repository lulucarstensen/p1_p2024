//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		handshake_pulse_sync 
//Date:	  		09/05/2024

// Handshake module to transmit fast clock signal to slow clock system 

module handshake_pulse_sync
(
	input signal_input,
	input clk_A,
	input clk_B,
	input reset,
	
	output busy,
	output signal_sync
);

localparam DW = 1;

wire mux_1_out_w;
wire mux_2_out_w;
wire reg_a1_out_w;
wire reg_b1_out_w;
wire reg_b2_out_w;
wire reg_b3_out_w;
wire reg_a2_out_w;
wire reg_a3_out_w;

mux_2_1
#(
	.DW(DW)  // Data width
)
mux_1
(
  	.sel(reg_a3_out_w),                
  	.dataA(reg_a1_out_w),  				   
  	.dataB(1'b0),            
   .dataOut (mux_1_out_w)   
);

mux_2_1
#(
	.DW(DW)  // Data width
)
mux_2
(
  	.sel(signal_input),                
  	.dataA(mux_1_out_w),  				   
  	.dataB(1'b1),            
   .dataOut (mux_2_out_w)   
);

siso_register #(1) reg_a1 (.clk(clk_A), .reset(reset), .sync_rst(1'b0), .DataIn(mux_2_out_w), .enable(1'b1), .DataOut(reg_a1_out_w));
siso_register #(1) reg_a2 (.clk(clk_A), .reset(reset), .sync_rst(1'b0), .DataIn(reg_b2_out_w), .enable(1'b1), .DataOut(reg_a2_out_w));
siso_register #(1) reg_a3 (.clk(clk_A), .reset(reset), .sync_rst(1'b0), .DataIn(reg_a2_out_w), .enable(1'b1), .DataOut(reg_a3_out_w));

siso_register #(1) reg_b1 (.clk(clk_B), .reset(reset), .sync_rst(1'b0), .DataIn(reg_a1_out_w), .enable(1'b1), .DataOut(reg_b1_out_w));
siso_register #(1) reg_b2 (.clk(clk_B), .reset(reset), .sync_rst(1'b0), .DataIn(reg_b1_out_w), .enable(1'b1), .DataOut(reg_b2_out_w));
siso_register #(1) reg_b3 (.clk(clk_B), .reset(reset), .sync_rst(1'b0), .DataIn(reg_b2_out_w), .enable(1'b1), .DataOut(reg_b3_out_w));

assign signal_sync = reg_b2_out_w & ~reg_b3_out_w;
assign busy = reg_a1_out_w | reg_a3_out_w;

endmodule
