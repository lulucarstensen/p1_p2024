// Project: QSYS
// Module:  QSYS - Module Instantiation (Structural Module)
// Author:  María Luisa Ortiz Carstensen and Victor Cervantes
// Date:    May 6th, 2024

module QSYS
#(
	parameter DW = 8,
	parameter DD = 16,
	parameter MW = 16,
	parameter NW = 4,
	parameter AB_DD = 4,
	parameter AB_M = 4,
	parameter AB_N = 4
)
(
	input clk_50Mhz,
	input clk_10Mhz,
	input rst,
	input enb,
	input [MW-1:0]fifo_input_write,
	input [MW-1:0][DW+AB_N-1:0]fifo_input_data,
	input [NW-1:0]read_fifo_out,
	output [NW-1:0]empty_fifo_out,
	output [NW-1:0][DW-1:0]fifo_output_data
);

wire [MW-1:0]empty_w;
wire [MW-1:0][AB_M-1:0]destiny_fifo_i;
wire [NW-1:0][MW-1:0]assign_read_w;

logic [NW-1:0]empty_fifo_output_l;
logic [NW-1:0][DW-1:0]fifo_output_data_l;

genvar i,j,k;

Interface_RR 
#(
	.DW(DW),
	.AW(AB_N),
	.MW(MW),
	.NW(NW)
) rr_interface(); 

generate

for(k = 0; k<MW ; k++)
begin: Inputs

   assign destiny_fifo_i[k] = (empty_w[k] == 0) ? rr_interface.RR.input_data[k][DW+AB_N-1:DW] : 12;
   
   FIFO
   #(
      .CLK_SYNC(1),
      .DW(AB_N+DW),
      .AW(DD), 
      .AB(AB_DD)
   ) fifo_input
   (
      .clk_rd(clk_50Mhz),
      .clk_wr(clk_50Mhz),
      .rst(rst),
      .rd_i(|rr_interface.input_FIFO.read[k]),
      .wr_i(fifo_input_write[k]),
      .data_i(fifo_input_data[k]),
	  
      .full(),
      .empty(empty_w[k]),
	  .data_o(rr_interface.input_FIFO.input_data[k])
   );
end

for(i = 0; i<NW ; i++)
begin:Outputs
   for(j = 0; j<MW ; j++)
   begin: read_requests
      assign rr_interface.input_FIFO.request[i][j] = ((destiny_fifo_i[j] == i) ? ~empty_w[j] : 0);
      assign rr_interface.RR.read[j][i] = assign_read_w[i][j];  
   end
   
   FIFO
   #(
      .CLK_SYNC(0),
      .DW(DW),
      .AW(DD), 
      .AB(AB_DD)    
   ) fifo_output
   (
      .clk_rd(clk_50Mhz),
      .clk_wr(clk_50Mhz),
      .rst(rst),
	  .rd_i(read_fifo_out[i]),
      .wr_i(rr_interface.output_FIFO.write[i]),
      .data_i(rr_interface.output_FIFO.output_data[i]),
	  
      .full(),
      .empty(empty_fifo_output_l[i]),
	  .data_o(fifo_output_data_l[i])
   );
   
   Round_Robin_Arbiter
   #(
      .NR(MW),
      .NW(NW),
      .DW(DW),
      .ADDR_W(AB_N),
      .ADDR_B(AB_M)
   ) arbiter
   (
      .clk(clk_50Mhz),
      .rst(rst),
      .enb(enb),
      .vec_request(rr_interface.input_FIFO.request[i]),
      .FIFO_data_i(rr_interface.RR.input_data),
	  
      .FIFO_data_o(rr_interface.RR.output_data[i]),
      .read(assign_read_w[i]),
	  .decoded_data(),
      .write(rr_interface.RR.write[i])
   );
end

endgenerate

assign fifo_output_data = fifo_output_data_l;
assign empty_fifo_out = empty_fifo_output_l;

endmodule