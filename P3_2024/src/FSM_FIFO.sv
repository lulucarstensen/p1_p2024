//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		FIFO State Machine
//Date:	  		May 8th, 2024

import data_pkg::*;


module FSM_FIFO
	#( 
		parameter ADDR_B = 8,
		parameter ADDR_W = 8
	)
(
	input 	clk,
	input 	rst,
	input 	enb,
	input 	wr, // push
	input 	rd, // pop
	input 	[ADDR_B - 1: 0] wr_addr,
	input 	[ADDR_B - 1: 0] rd_addr,
	
	output 	full,
	output	empty,
	output	we,
	output 	rd_o, // pop
	output 	wr_syncrst,
	output	rd_syncrst
);
	
	
// Logic Variables	

logic full_l;
logic empty_l;
logic we_l;
logic rd_l;
logic wr_syncrst_l;
logic rd_syncrst_l;
logic wr_or_rd_addr_l;


FIFO_state current_state;
FIFO_state next_state;


// Clock and Reset Logic 
always_ff @(posedge clk or negedge rst)
begin
		if(rst == 1'b0)
			current_state <= EMPTY;
		
		else if(enb == 1'b1)
			current_state <= next_state;
end


// Combinational Section, INPUTS
always_comb begin

	case(current_state)
	
		IDLE: begin
				
				if(rd_addr == wr_addr)begin
						
						if(wr == 1'b1)begin
								next_state = FULL;
						end
						
						else 
								next_state = EMPTY;
				end
				else 
						next_state = IDLE;
				
		end
		
		EMPTY: begin
				
				if(wr == 1'b1)
						next_state = IDLE;
				
				else 
						next_state = EMPTY;
				
		end
		
		FULL: begin
				
				if(rd == 1'b1)
						next_state = IDLE;
						
				else 		
						next_state = FULL; 
		
		end
		
		default: begin
				next_state = IDLE;
		end
	endcase	
end 


// Combinational Section, OUTPUTS
always_comb begin

	case(current_state)
	
		IDLE: begin
				
				// Conditions per output:
					
					//		 * full *
					
							if((rd_addr == wr_addr) && (wr == 1'b1))begin
									full_l = 1'b1;
							end		
							else begin 
									full_l = 1'b0;
							end
							
					// 	* empty *
					
							if((rd_addr == wr_addr) && (rd == 1'b1))begin
									empty_l = 1'b1;
							end		
							else begin 
									empty_l = 1'b0;
							end		
							
					//		 * we *
					
							if((rd_addr == wr_addr) && (wr == 1'b1))begin
									we_l = 1'b1;
							end		
							else begin 
									we_l = 1'b0;
							end		
									
					// 	* rd * 
					
							if((rd_addr == wr_addr) && (rd == 1'b1))begin
									rd_l = 1'b1;
							end		
							else begin 
									rd_l = 1'b0;
							end		
									
					// 	* wr_syncrst * 
					
							if((ADDR_W - 1) == wr_addr)begin
									wr_syncrst_l = 1'b1;
							end		
							else begin
									wr_syncrst_l = 1'b0;
							end		

					// 	* rd_syncrst *
												
							if((ADDR_W - 1) == rd_addr)begin
									rd_syncrst_l = 1'b1;
							end		
							else begin
									rd_syncrst_l = 1'b0;
							end
	end
		
		EMPTY: begin
				
				full_l			= 1'b0;
				empty_l			= 1'b1;
				we_l				= 1'b0;
				rd_l				= 1'b1;
				wr_syncrst_l	= 1'b0;
				rd_syncrst_l	= 1'b0;			
				
		end
		
		FULL: begin

				full_l			= 1'b1;
				empty_l			= 1'b0;
				we_l				= 1'b1;
				rd_l				= 1'b0;
				wr_syncrst_l	= 1'b0;
				rd_syncrst_l	= 1'b0;		
				
		end
		
		default: begin
		
				full_l			= 1'b1;
				empty_l			= 1'b1;
				we_l				= 1'b1;
				rd_l				= 1'b0;
				wr_syncrst_l	= 1'b0;
				rd_syncrst_l	= 1'b0;
		end	
	endcase
end

assign full 		= full_l;
assign empty 		= empty_l;
assign we 			= we_l;
assign rd_o 		= rd_l;
assign wr_syncrst = wr_syncrst_l;
assign rd_syncrst = rd_syncrst_l;

endmodule 