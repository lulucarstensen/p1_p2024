//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		Arbiter SubModule
//Date:	  		May 8th, 2024

import data_pkg::*;

module Arbiter_Submodule
#(		
		parameter DW = 16,
		parameter AB = 4
)
(
	input clk,
	input rst,
	input enb,
	input [DW - 1: 0] data_request,
	
	output [AB - 1: 0] decoded_data
);

//*		 WIRES			*//

wire [AB - 1: 0] decoded_data_w;
wire [DW - 1: 0] PPC_w;
wire [DW - 1: 0] vec_req_mux_w;
wire [DW - 1: 0] shift_w;
wire [DW - 1: 0] vec_req_pipo_w;


//*	  MODULE INSTANTIATION		*//


binary_decoder_DW #( .DW(DW), .AB(AB) )	decoder 
(
	.data_input(PPC_w ^ shift_w),
	
	.data_output(decoded_data_w)
);


shift_right_left #( .DW(DW) ) data_shift
(
	.right_or_left(1), //right = 0, left = 1
	.data_to_shift(PPC_w),
	
	.shifted_o(shift_w)	
);

	
ppc #( .MW(DW) ) PPC
(
	.data_input(data_request & vec_req_pipo_w),
	
	.data_output(PPC_w)
);
	
	
	
pipo #( .DW(DW)) vector_request_PIPO
(
		.clk(clk),
		.rst(rst),
		.enb(enb),
		.sync_rst(0), //OFF 
		.inp(vec_req_mux_w),

		.out(vec_req_pipo_w)
);


mux_2_1 #( .DW(DW) ) vector_request_mux
(
	  .sel	(|( shift_w & ( data_request & vec_req_pipo_w ) )),	// |(Shift & PPC)
	  .dataA	(data_request),	// Input (N x DW bits)
	  .dataB	( shift_w & ( data_request & vec_req_pipo_w ) ), // Shift & PPC
	  
	  .dataOut(vec_req_mux_w)	//Output (Data Width)
);

assign decoded_data = decoded_data_w;

endmodule		
		