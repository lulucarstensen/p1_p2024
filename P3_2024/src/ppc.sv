//Author:  		Victor Cervantes
//Project: 		QSYS
//Module:  		Parallel Prefix Computation Unit 
//Date:	  		May 8th, 2024

module ppc #(parameter MW = 16)
(
	input [MW-1:0] data_input,
	output [MW-1:0] data_output
);

logic [MW-1:0] data_output_l;

always_comb begin
	data_output_l = ( data_input | (~(data_input)+1'b1) );
end

assign data_output = data_output_l;

endmodule
