// Project: QSYS
// Module:  RAM Dual Port
// Author:  María Luisa Ortiz Carstensen
// Date:    May 6th, 2024
// Reference:	https://www.intel.com/content/www/us/en/support/programmable/support-resources/design-examples/horizontal/ver-single-clock-syncram.html



module ram_dual#(
   parameter DW = 8, //Data Width
   parameter AW = 8, //Addres Width
   parameter AB = 3 //Address bits, it must be log2(AW)+1 as minimum.
)
(
	input [DW-1:0] data,
	input [AB-1:0] read_addr, write_addr,
	input we, clk,
	
	output logic [DW-1:0] q // Data Output
);

// Declare the RAM variable
logic [DW-1:0] ram[AW-1:0];
logic [DW-1:0]q_l;	
	
	
always_ff@(posedge clk)
	begin
		// Write
		if (we)
			ram[write_addr] <= data;
	end
	
	
// Read (if read_addr == write_addr, return OLD data).	To return
// NEW data, use = (blocking write) rather than <= (non-blocking write)
// in the write assignment.	 NOTE: NEW data may require extra bypass
// logic around the RAM.

always_comb
	begin
		q_l = ram[read_addr];
	end
	
assign q = q_l;
	
endmodule

