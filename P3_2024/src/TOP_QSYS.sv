// Project: QSYS
// Module:  QSYS - Module Instantiation (Structural Module)
// Author:  María Luisa Ortiz Carstensen ans Victor Cervantes
// Date:    May 6th, 2024

module TOP_QSYS
#(
	parameter DW = 8,
	parameter DD = 64,
	parameter MW = 16,
	parameter NW = 8,
	parameter AB_DD = 6,
	parameter AB_M = 7,
	parameter AB_N = 4,
	parameter SIMULATION = 0 //0 real, 1 tb
)
(
	input clk,
	input rst,
	input sync_rst_i,
	input uart_rx_serial,
	output uart_tx_serial
);

// UART wires 

wire clk_10Mhz_w;
wire [DW-1:0]uart_rx_data_w;
wire uart_rx_ready_w;
wire [DW-1:0]uart_tx_data_w;
wire uart_tx_ready_w;
wire start_tx_uart_w;

//FSM signals
wire sync_rst;
wire fifo_enb_w; // to be removed
wire enable_fifo_i_w;  // to be removed
wire enb_rr_qsys_w;
wire enable_rr_fsm_w;
wire length_pipo_enb_w;
wire dest_pipo_enb_w;
wire src_pipo_enb_w;
wire mask_fifos_i_enb_w;
wire enb_clear_of_w;
wire counter_enb_w;
wire counter_sync_rst_w;
wire next_w;
wire transmit_next_w;
wire mask_send_w;
logic read_l;
wire send_w;

//data vectors
wire [AB_N-1:0] dst_pipo_out_w;
wire [AB_M-1:0] src_pipo_out_w;
wire [DW-1:0]length_pipo_out_w;
wire [MW-1:0]fifo_input_write_w;
wire [MW-1:0][DW+AB_N-1:0]fifo_input_data_w;
wire [DW-1:0]output_counter_w;
wire [NW-1:0] read_fifo_output_w;
wire [AB_N-1:0] address_fifo_o_w;
<<<<<<< HEAD
=======

>>>>>>> ec851873945c131b9fbd515481a2b03e24413848

logic [AB_M-1:0] mask_out_w;
logic [NW-1:0]empty_fifo_output_l;
logic [NW-1:0][DW-1:0]fifo_output_data_l;

<<<<<<< HEAD
=======

>>>>>>> ec851873945c131b9fbd515481a2b03e24413848
QSYS
#(
	.DW(DW),
	.DD(DD),
	.MW(MW),
	.NW(NW),
	.AB_DD(AB_DD),
	.AB_M(AB_M),
	.AB_N(AB_N)
) QSYS
(
	.clk_50Mhz(clk),
	.clk_10Mhz(clk_10Mhz_w),
	.rst(rst),
	.enb(enb_rr_qsys_w), //round robin enb
	.fifo_input_write(fifo_input_write_w),
	.fifo_input_data(fifo_input_data_w),
	.read_fifo_out(read_fifo_output_w),
	.empty_fifo_out(empty_fifo_output_l),
	.fifo_output_data(fifo_output_data_l)
);

counter
#(
	.DW(DW)
) counter
(
	.enb(counter_enb_w),
	.clk(clk),
	.rst(rst),
	.sync_rst(counter_sync_rst_w),
	.count_o(output_counter_w)
);

FSM_commands 
#(
	.DW(DW),
	.AW(AB_N)
) FSM_commands_top
(  
    .clk(clk),
    .rst(rst),
    .enb(1'b1),
    .count_i(output_counter_w),
    .command_i(uart_rx_data_w),
	.next(next_w),
	.empty_i(|empty_fifo_output_l),
    .tx_next(transmit_next_w),
    .length_i(length_pipo_out_w),
	.fifo_enb(enable_fifo_i_w), //signal to be removed
	.arbiter_enb(enable_rr_fsm_w),
    .write(read_l),
	.clear_out_enb(enb_clear_of_w),
	.counter_enb(counter_enb_w),
	.counter_sync_rst(counter_sync_rst_w),
	.fifo_mask(mask_fifos_i_enb_w),
    .length_o(length_pipo_enb_w),
	.source(src_pipo_enb_w),
	.destination(dest_pipo_enb_w),
    .mask_send(mask_send_w)
);

// siso reg to be removed
pipo 
#(
	.DW(1)
) pipo_enable_fifo_i
(
	.clk(clk),
	.rst(rst),    
	.enb(enable_fifo_i_w),
	.sync_rst(sync_rst),
	.inp(2'b11),
	.out(fifo_enb_w)
);
// ends instantiation siso reg

pipo 
#(
	.DW(1)
) pipo_enable_rr
(
	.clk(clk),
	.rst(rst),
	.enb(enable_rr_fsm_w),
	.sync_rst(sync_rst),
	.inp(2'b11),
	.out(enb_rr_qsys_w)
);

pipo 
#(
	.DW(DW)
) pipo_length
(
	.clk(clk),
	.rst(rst),
	.enb(length_pipo_enb_w),    
	.sync_rst(sync_rst), 
	.inp(uart_rx_data_w),
	.out(length_pipo_out_w)
);

pipo 
#(
	.DW(AB_M)
) pipo_src
(
	.clk(clk),
	.rst(rst),
	.enb(src_pipo_enb_w),    
	.sync_rst(sync_rst), 
	.inp(uart_rx_data_w),
	.out(src_pipo_out_w)
);

pipo 
#(
	.DW(AB_N)
) pipo_dst
(
	.clk(clk),
	.rst(rst),
	.enb(dest_pipo_enb_w),    
	.sync_rst(sync_rst),
	.inp(uart_rx_data_w),
	.out(dst_pipo_out_w)
);

mask
#(
	.DW(DW)
) mask_fifos
(
	.clk(clk),
	.rst(rst),
	.enb(mask_fifos_i_enb_w),
	.mask_data_i(uart_rx_data_w),
	.mask_data_o(mask_out_w)
);

one_shot next
(
	.rst(rst),
	.clk(clk),
	.pb_i(uart_rx_ready_w),
	.pulse_o(next_w)
);

one_shot transmit_next
(
	.rst(rst),
	.clk(clk),
	.pb_i(uart_tx_ready_w & mask_send_w),
	.pulse_o(transmit_next_w)
);

clear_output_fifos
#(
   .FIFOS(NW),
   .FB(AB_N)
) clear_fifos_unit
(
	.enb(enb_clear_of_w),
	.clk(clk),
	.rst(rst),
	.request_fifos(~empty_fifo_output_l),
	.available_TX(uart_tx_ready_w),
	.address(address_fifo_o_w),
	.read_fifo(read_fifo_output_w),
	.send_data(send_w)
);

handshake_pulse_sync uart_handshake
(
    .signal_input(send_w),
    .clk_A(clk),
    .clk_B(clk_10Mhz_w),
	.reset(rst),
    .busy(/*Disconnected*/),
	.signal_sync(start_tx_uart_w)
);

generate
    case(SIMULATION)
        0:begin
		  
            PLL_QSYS_10M PLL_10M(
               .areset(~rst),
               .inclk0(clk),
               .c0(clk_10Mhz_w),
               .locked(/*Disconnected*/)
            );

				UART #(
					.DW(DW)
				) UART
				(
					.clk(clk_10Mhz_w),
					.rst(rst),
					.enb(1'b1),
					.start_tx(start_tx_uart_w && (empty_fifo_output_l[address_fifo_o_w] != 1'b1)),
					.data_to_tx(fifo_output_data_l[address_fifo_o_w]),
					.tx_serial(uart_tx_serial),
					.tx_ready(uart_tx_ready_w),
					.rx_serial(uart_rx_serial),
					.rx_data(uart_rx_data_w),
					.rx_ready(uart_rx_ready_w)
				);
        end
        1:begin
				assign clk_10Mhz_w = clk;
            UART #(
					.DW(DW)
				) UART
				(
					.clk(clk_10Mhz_w),
					.rst(rst),
					.enb(1'b1),
					.start_tx(start_tx_uart_w && (empty_fifo_output_l[address_fifo_o_w] != 1'b1)),
					.data_to_tx(fifo_output_data_l[address_fifo_o_w]),
					.tx_serial(uart_tx_serial),
					.tx_ready(uart_tx_ready_w),
					.rx_serial(uart_rx_serial),
					.rx_data(uart_rx_data_w),
					.rx_ready(uart_rx_ready_w)
				);
        end
    endcase
endgenerate

genvar i;

generate
for(i = 0; i<MW; i++)
begin: fifo_source
	assign fifo_input_data_w[i] = {(dst_pipo_out_w), uart_rx_data_w};
	assign fifo_input_write_w[i] = ((src_pipo_out_w & mask_out_w) == i) ? read_l : 1'b0;
end: fifo_source

endgenerate

assign sync_rst = ~sync_rst_i;

endmodule