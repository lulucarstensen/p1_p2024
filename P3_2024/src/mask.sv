//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		Mask for FIFOs' to determine direction
//Date:	  		May 8th, 2024


module mask 
	#(parameter DW = 8)
(
	input clk,
	input rst,
	input enb,
	input [DW - 1: 0] mask_data_i,
	
	output [DW - 1: 0] mask_data_o
);

logic [DW - 1: 0] data_o_l;

always_comb begin
	
	case(mask_data_i)

		8'b0000_0001: data_o_l = 8'b0000_0000;
		8'b0000_0010: data_o_l = 8'b0000_0001;
		8'b0000_0100: data_o_l = 8'b0000_0011;
		8'b0000_1000: data_o_l = 8'b0000_0111;
		8'b0001_0000: data_o_l = 8'b0000_1111;
		8'b0010_0000: data_o_l = 8'b0001_1111;
		8'b0100_0000: data_o_l = 8'b0011_1111;
		8'b1000_0000: data_o_l = 8'b0111_1111;
		
		default: data_o_l = 8'b0000_0000;
		
	endcase
end

// Instantiate with PIPO Register

pipo 
	#(.DW(DW)) 
pipo_mask
(
	.clk(clk),
	.rst(rst),
	.enb(enb),
	.sync_rst(0),		// Disabled 
	.inp(mask_data_i),
	
	.out(mask_data_o)
);
	
endmodule 