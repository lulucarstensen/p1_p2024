//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		Sequential Multiplier
//Module:  		Register 
//Date:	  		13/02/2024

//Module to create a register according to paramater N equals Data Width

module siso_register #(parameter N = 1)
(

    input 	clk,
    input	reset,
    input 	enable,
	 input 	sync_rst,
    input   [N-1:0] DataIn,

    output reg [N-1:0] DataOut
);

always_ff@(posedge clk or negedge reset) begin 
	if(reset == 1'b0)
        DataOut <= 1'b0000; //reset sets DataOut to 0
   else begin
		if(enable == 1'b1) begin
			if (sync_rst)
				DataOut <= 1'b0000; //reset sets DataOut to 0		
			else 
				DataOut <= DataIn;
		end
	end
end
 
endmodule