//Modified by: Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		Round Robin Arbiter 
//Date:	  		May 8th, 2024
//Reference: 	[Arbiter] -> https://circuitcove.com/design-examples-rr-arbiter/
//Reference:	[Generate] -> https://www.systemverilog.io/verification/generate/



module Round_Robin_Arbiter
#(
			parameter NR = 8, // Number of Requests
			parameter NW = 8,
			parameter DW = 8,
			parameter ADDR_W = 8,
			parameter ADDR_B = 4
)
(
	input clk,
	input rst,
	input enb,
	input [NR - 1: 0] vec_request,
	input [NR - 1: 0] [ (DW+ADDR_W-1) : 0] FIFO_data_i,
	
	output [DW - 1: 0] FIFO_data_o,
	output [NR - 1: 0] read, 
	output [ADDR_B - 1:0] decoded_data,
	output write
);

wire [ADDR_B - 1: 0] data_l;

  
generate 
	genvar i;
		for(i = 0; i < NR ; i++)
			begin: FIFO_iteration
			assign read[i] = ((enb == 1'b1) && (data_l == i) && vec_request[i] != 0) ? 1'b1 : 1'b0;
			end
endgenerate



Arbiter_Submodule #(
    .DW(DW),
    .AB(ADDR_B)
) arb_submodule (
    .clk(clk),
    .rst(rst),
    .enb(enb),
    .data_request(vec_request),

    .decoded_data(data_l)
);

assign decoded_data = data_l;
assign FIFO_data_o = FIFO_data_i[data_l][DW-1:0];
assign write = (enb == 1'b1) ? (vec_request[data_l]) : 1'b0; 


endmodule

