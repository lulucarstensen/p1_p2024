//Author:  		Victor Cervantes
//Project: 		QSYS
//Module:  		parameterizable binary decoder to with for rr arbiter
//Date:	  		May 8th, 2024

module binary_decoder_DW
#(
	parameter DW = 16,
	parameter AB = 4
)
(
	input [DW-1:0] data_input,
	output [AB-1:0] data_output
);

genvar i;
logic [AB-1:0]count_l[DW-1:0];
logic [AB-1:0]result_l[DW-1:0];


generate
   assign count_l[0] = 0;
   assign result_l[0] = 0;
	for(i = 1; i < DW; i++)
   begin: calculation
		assign count_l[i] = count_l[i-1]+1;
		assign result_l[i] = (data_input[i] == 1'b1) ? result_l[i-1] + count_l[i] : result_l[i-1];
   end: calculation
endgenerate

assign data_output = result_l[DW-1]; 

endmodule