
State Machine - |TOP_QSYS|UART:UART|UART_Rx:Rx_Top|FSM_UART:fsm|current_state
Name current_state.PARITY current_state.DATA current_state.START current_state.IDLE current_state.DONE 
current_state.IDLE 0 0 0 0 0 
current_state.START 0 0 1 1 0 
current_state.DATA 0 1 0 1 0 
current_state.PARITY 1 0 0 1 0 
current_state.DONE 0 0 0 1 1 

State Machine - |TOP_QSYS|UART:UART|uart_tx:Tx_Top|FSM_uart_tx:uart_fsm|current_state
Name current_state.STATE_IDLE current_state.STATE_DATA_TX current_state.STATE_FRAME_CONSTRUCTION 
current_state.STATE_IDLE 0 0 0 
current_state.STATE_FRAME_CONSTRUCTION 1 0 1 
current_state.STATE_DATA_TX 1 1 0 

State Machine - |TOP_QSYS|clear_output_fifos:clear_fifos_unit|FSM_clear_OF:fsm_clr_output_fifos|current_state
Name current_state.IDLE_CO current_state.READ current_state.TRANSMIT 
current_state.IDLE_CO 0 0 0 
current_state.TRANSMIT 1 0 1 
current_state.READ 1 1 0 

State Machine - |TOP_QSYS|FSM_commands:FSM_commands_top|current_state
Name current_state.RX_IDLE current_state.ERROR current_state.EOF current_state.RX current_state.ARBITER_ENB current_state.FILL current_state.FILL_BEGIN current_state.PORT_SETTING current_state.COMMAND current_state.LENGTH current_state.IDLE_CMD 
current_state.IDLE_CMD 0 0 0 0 0 0 0 0 0 0 0 
current_state.LENGTH 0 0 0 0 0 0 0 0 0 1 1 
current_state.COMMAND 0 0 0 0 0 0 0 0 1 0 1 
current_state.PORT_SETTING 0 0 0 0 0 0 0 1 0 0 1 
current_state.FILL_BEGIN 0 0 0 0 0 0 1 0 0 0 1 
current_state.FILL 0 0 0 0 0 1 0 0 0 0 1 
current_state.ARBITER_ENB 0 0 0 0 1 0 0 0 0 0 1 
current_state.RX 0 0 0 1 0 0 0 0 0 0 1 
current_state.EOF 0 0 1 0 0 0 0 0 0 0 1 
current_state.ERROR 0 1 0 0 0 0 0 0 0 0 1 
current_state.RX_IDLE 1 0 0 0 0 0 0 0 0 0 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[7].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[6].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[5].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[4].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[3].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[2].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[1].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Outputs[0].fifo_output|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[15].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[14].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[13].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[12].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[11].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[10].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[9].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[8].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[7].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[6].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[5].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[4].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[3].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[2].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[1].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 

State Machine - |TOP_QSYS|QSYS:QSYS|FIFO:Inputs[0].fifo_input|FSM_FIFO:fsm_fifo|current_state
Name current_state.IDLE current_state.FULL current_state.EMPTY 
current_state.EMPTY 0 0 0 
current_state.IDLE 1 0 1 
current_state.FULL 0 1 1 
