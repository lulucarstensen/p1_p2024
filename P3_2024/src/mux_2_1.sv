//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		Mux 2 to 1 
//Date:	  		May 8th, 2024

module mux_2_1 #(parameter DW = 10)
(
  input   bit  		  		  sel,	// Mux Selector
  input   [DW - 1 : 0]		dataA,	// Input (N x DW bits)
  input   [DW - 1 : 0]		dataB,
  
  output  [DW - 1 : 0]	  dataOut	//Output (Data Width)
);
	
 assign dataOut = (sel == 0) ? dataA : dataB;
 
endmodule 