// Project: UART
// Module:  UART
// Author:  María Luisa Ortiz Carstensen and Victor Cervantes
// Date:    March 29th, 2024

module UART
#(
	parameter DW = 8
  )
(
	input clk,
	input rst,
	input enb,
	
	input start_tx,
	input [DW-1:0]data_to_tx,
	output tx_serial,		// Tx serial Data 
	output tx_ready,
	
	input rx_serial,		// Rx serial Data
	output [DW-1:0]rx_data,
	output rx_ready
);

// WIRES
wire tx_ready_w;
wire tx_frame_w;
wire [DW-1:0]rx_parallel_data_w;
wire rx_ready_w;

// Tx Instance
uart_tx #(.DW(DW)) Tx_Top 
(
    .rst(rst),
    .sync_rst(0), // syncrst_w
    .clk(clk), // Use the correct clock signal
    .start_tx(start_tx),
    .even_odd(0), // 0 for even, 1 for odd
    .parity(1),   // Parity active
    .data_to_send(data_to_tx),
	 
    .frame(tx_frame_w),
    .ready(tx_ready_w)
);

// Rx Instance
UART_Rx #(.DW(DW)) Rx_Top
(
	.clk(clk),
	.enb(enb),
	.rx_i(rx_serial),
	.rst(rst),
	
	.rx_o(rx_parallel_data_w),
	.rx_ready(rx_ready_w)
);

assign tx_serial = tx_frame_w;
assign tx_ready = tx_ready_w; 
assign rx_data = rx_parallel_data_w;
assign rx_ready = rx_ready_w;

endmodule 