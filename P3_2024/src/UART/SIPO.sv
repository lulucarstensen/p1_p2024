// Project: UART ECHO
// Module:  UART Rx - SIPO
// Author:  María Luisa Ortiz Carstensen, Abisaí 
// Date:    March 30th, 2024



module SIPO #(parameter DW = 8)
(
	input bit           clk,
	input bit           rst,
	input logic         enb,
	input logic         inp,
	output  [DW-1:0]    out
);

logic [DW-1:0]      rgstr_r;

always_ff@(posedge clk or negedge rst) begin: rgstr_sipo
    if(!rst)
        rgstr_r  <= '0;
    else if (enb)
        rgstr_r  <= {rgstr_r[DW-2:0], inp};
end:rgstr_sipo

assign out  = rgstr_r;

endmodule
