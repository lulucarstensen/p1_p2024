// Project: UART ECHO
// Module:  UART Tx - FSM Tx
// Author:  Víctor Cervantes Gómez
// Date:    March 30th, 2024


module FSM_uart_tx(
	input clk,
	input clkdiv,			// Clock division signal
	input reset,
	input ov_cntov,		// Overflow count signal
	input start_tx,		// Start transmission signal
	input ov_cnt5,			// Overflow count for 5 bits
	input syncreset_i,
	input [3:0]ov_count,	// Overflow count signal
	
	output load_or_shift,
	output en_piso,
	output en_cnt_ov,				// Signal for enabling overflow count
	output en_cnt5,				// Signal for enabling count of 5 bits
	output syncreset_o,
	output ready_o,
	output mux_sel_o
);


typedef enum logic[1:0]{
	STATE_IDLE = 2'b00,
	STATE_FRAME_CONSTRUCTION = 2'b01,
	STATE_DATA_TX = 2'b10
} fsm_state_type;

fsm_state_type current_state;
fsm_state_type next_state;

logic load_or_shift_l;
logic en_piso_l;
logic en_cnt_ov_l;
logic syncreset_o_l;
logic ready_o_l;
logic mux_sel_o_l;
logic en_cnt5_l;

logic [3:0] internal_count;


// First combinational section

always_comb begin
	case(current_state)
	STATE_IDLE: begin //IDLE 
		if(start_tx == 1'b1)
			next_state = STATE_FRAME_CONSTRUCTION;
		else 
			next_state = STATE_IDLE;
	end
	STATE_FRAME_CONSTRUCTION:begin //ENABLE
		if(ov_cnt5)
			next_state = STATE_DATA_TX;
		else
			next_state = STATE_FRAME_CONSTRUCTION;
	end
	STATE_DATA_TX: begin //
		//if(ov_cntov) ov_count
		if(ov_count == 4'b1011) 
			next_state = STATE_IDLE;
		else 
			next_state = STATE_DATA_TX;
	end
	default: begin
		next_state = STATE_IDLE;
	end
	endcase
end
// Sequential section

always_ff @(posedge clk, negedge reset) begin

	if(!reset)
		current_state <= STATE_IDLE;
	else if(syncreset_i == 1'b1) begin
		current_state <= STATE_IDLE;
		if(internal_count == 4'b1101)
			internal_count  <= 4'b0000;
		else
			internal_count <= internal_count + 1;
		end
	else
		current_state <= next_state;
end

// Second combinational section
always_comb begin

	case(current_state)
	STATE_IDLE: begin
		load_or_shift_l = 1'b1;
		en_piso_l = 1'b0;
		en_cnt_ov_l = 1'b0;
		syncreset_o_l = 1'b1;
		ready_o_l = 1'b1;
		mux_sel_o_l = 1'b1;
		en_cnt5_l = 1'b0; 
	end
	STATE_FRAME_CONSTRUCTION: begin
		load_or_shift_l = 1'b1;
		en_piso_l = 1'b1;
		//en_cnt_ov_l = (ov_cnt5) ? 1'b1 : 1'b0 ; 
		en_cnt_ov_l = 1'b0;
		syncreset_o_l = 1'b0;
		ready_o_l = 1'b0;
		//mux_sel_o_l = (clkdiv) ? 1'b0 : 1'b1;
		mux_sel_o_l = 1'b1;
		en_cnt5_l = 1'b1; 
	end
	STATE_DATA_TX: begin
		load_or_shift_l = 1'b0;
		en_piso_l = (ov_cnt5) ? 1'b1 : 1'b0;
		en_cnt_ov_l = (ov_cnt5) ? 1'b1 : 1'b0;
		syncreset_o_l = 1'b0;
		ready_o_l = 1'b0;
		mux_sel_o_l = 1'b0;
		en_cnt5_l = 1'b1;
	end	
	default: begin
		load_or_shift_l = 1'b1;
		en_piso_l = 1'b0;
		en_cnt_ov_l = 1'b1;
		syncreset_o_l = 1'b0;
		ready_o_l = 1'b0;
		mux_sel_o_l = 1'b1;
		en_cnt5_l = 1'b1;
		end
	endcase
	
end

// Output assignment section
assign load_or_shift = load_or_shift_l;
assign en_piso = en_piso_l;
assign en_cnt_ov = en_cnt_ov_l;
assign syncreset_o = syncreset_o_l;
assign ready_o = ready_o_l;
assign mux_sel_o = mux_sel_o_l;
assign en_cnt5 = en_cnt5_l;

endmodule