// Project: UART ECHO
// Module:  UART Tx - Parity
// Author:  Víctor Cervantes Gómez
// Date:    March 30th, 2024

module parity_mod
(
	input clk, 
	input reset, 
	input sync_rst,
	input even_odd_i,
	input [7:0]data_in,  
	output [8:0] data_out
);

logic [2:0] sum_w;
logic pair_w;
logic [8:0] parity_frame;

always_comb begin

	sum_w = data_in[0] + data_in[1] + data_in[2] + data_in[3] + data_in[4] + data_in[5] + data_in[6] + data_in[7]; 
	pair_w = (sum_w % 2 == 0) ? 1'b1 : 1'b0;
	
	case (even_odd_i) // 0 for even - 1 for odd
		1'b0: begin
			if(pair_w)
				parity_frame = {data_in,1'b0};
			else
				parity_frame = {data_in,1'b1};
		end
		1'b1: begin
			if(pair_w)
				parity_frame = {data_in,1'b1};
			else
				parity_frame = {data_in,1'b0};
		end
		default: begin
			parity_frame = {data_in,1'b0};
			end
	endcase
end

assign data_out = parity_frame;

endmodule
