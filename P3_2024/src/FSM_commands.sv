//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		QSYS
//Module:  		FSM_COMMANDS for QSYS control
//Date:	  		May 8th, 2024

import data_pkg::*;

module FSM_commands 
#(
		parameter DW = 8,
		parameter AW = 4
)
(
		input clk,
		input rst,
		input enb,
		input [DW - 1 : 0] count_i,
		input [DW - 1 : 0] command_i,
		input next,
		input empty_i,
		input tx_next,
		input [DW - 1 : 0] length_i,
		
		 output fifo_enb,
		 output arbiter_enb,
		 output write,
		 output clear_out_enb,
		 output counter_enb,
		 output counter_sync_rst,
		 output fifo_mask,
		 output length_o,
		 output source,
		 output destination,
		 output mask_send
);

cmd_state current_state;
cmd_state next_state;

// Logic Variables

logic fifo_enb_l;
logic arbiter_enb_l;
logic write_l;
logic clear_out_enb_l;
logic counter_enb_l;
logic counter_sync_rst_l;
logic fifo_mask_l;
logic length_o_l;
logic source_l;
logic destination_l;
logic mask_send_l;


// Clock and Reset Logic 
always_ff @(posedge clk or negedge rst)
begin
		if(rst == 1'b0)
			current_state <= IDLE_CMD;
		
		else if(enb == 1'b1)
			current_state <= next_state;
end


// Combinational Section, INPUTS
always_comb begin
	
	case(current_state)
	
			
			IDLE_CMD:begin
				
				if(next && (command_i == 8'hFE) )
						next_state = LENGTH;
				else 
						next_state = IDLE_CMD;
					
			end	
			
			
			LENGTH:begin
			
				if( !next )
						next_state = LENGTH;
				
				else if(command_i >= 8'h02)
						next_state = COMMAND;
				else 
						next_state = ERROR;
			end	

			
			COMMAND:begin
			
				if (!next )
						next_state = COMMAND;
						
				else if(command_i == 8'h01)
						next_state = PORT_SETTING;
				
				else if(command_i == 8'h02)
						next_state = RX;
						
				else if(command_i == 8'h03)
						next_state = FILL_BEGIN;
						
				else if(command_i == 8'h04)
						next_state = FILL;
				
				else if(command_i == 8'h05)
						next_state = ARBITER_ENB;
						
				else 
						next_state = ERROR;
			end	

			
			PORT_SETTING:begin
			
				if(!next)
						next_state = PORT_SETTING;
						
				else if((length_i != 8'h03 && (command_i != 8'b00000000) || length_i != 8'h03 && (command_i != 8'b00000001 || command_i != 8'b0000_0010 || command_i != 8'b00000100 || command_i != 8'b00001000 || command_i != 8'b00010000 || command_i != 8'b00100000 || command_i != 8'b01000000 || command_i != 8'b10000000)) )
						next_state = ERROR;
							
				else
						next_state = EOF;
			 end

			 
			FILL_BEGIN:begin
			
				if(!next)
						next_state = FILL_BEGIN;
				
				else if(length_i != 8'h02 && command_i == 8'hEF)
						next_state = ERROR;
						
				else 
						next_state = IDLE_CMD; 
			
			end	
		
		
			FILL:begin
			
				if(!next)
						next_state = FILL;
						
				else if(length_i >= 8'h04)
						next_state = ERROR;
						
				else if(count_i == length_i -1)
						next_state = EOF;
				else 
						next_state = FILL;
			
			end	
				
				
			ARBITER_ENB:begin
			
				if(!next)
						next_state = ARBITER_ENB;
						
				else if((length_i != 8'h02) && (command_i == 8'hEF))
						next_state = 	ERROR;
				
				else 
						next_state = IDLE_CMD;
				
			end	
				
				
			RX:begin
			
				if(!next)
						next_state = RX;
						
				else if(length_i != 8'h02 && command_i == 8'hEF)
						next_state = ERROR;
						
				else 
						next_state = RX_IDLE;
			end	
						
						
			EOF:begin
			
				if(!next)
						next_state = EOF;
				
				else if(command_i == 8'hEF && count_i == length_i)
						next_state = IDLE_CMD;
						
				else
						next_state = ERROR;	
			end	
						
						
			ERROR:begin
			
				if(next && command_i == 8'hFE)
						next_state = LENGTH;
						
				else
						next_state = ERROR;
			
			end	
						
	      RX_IDLE:begin
			
				if(next && command_i == 8'hFE)
						next_state = LENGTH;
						
				else
						next_state = RX_IDLE; 
			
			end	
			
			default:begin
						next_state = IDLE_CMD;
			end
			
	endcase
	
end



// Combinational Section, OUTPUTS
always_comb begin
	
	case(current_state)
	
			
			IDLE_CMD:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= 1'b1;
					 counter_sync_rst_l	= 1'b1;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;
			
			end	
			
			LENGTH:begin

					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= (next) ? 1'b1: 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;			
			end	
					
			COMMAND:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;					
			
			end	
					
			PORT_SETTING:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= 1'b0;
					 fifo_mask_l			= (next) ? 1'b1: 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;
			
			end	
			
			FILL_BEGIN:begin
			
					 fifo_enb_l				= (next) ? 1'b1: 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= (next) ? 1'b1: 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;			
			
			end	
				
			FILL:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= (next && count_i > 8'h03) ? 1'b1 : 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= (next && count_i == 8'h02) ? 1'b1 : 1'b0;
					 destination_l			= (next && count_i == 8'h03) ? 1'b1 : 1'b0;
					 mask_send_l			= 1'b0;				
			
			end	
						
			ARBITER_ENB:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= (next) ? 1'b1: 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= (next) ? 1'b1: 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;
			
			end	
				
			RX:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= (next) ? 1'b1: 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;			
			
			end	
							
			EOF:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= (next) ? 1'b1: 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;				
			
			end	
						
			ERROR:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= (next) ? 1'b1: 1'b0;
					 counter_sync_rst_l	= (next) ? 1'b1: 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;				
			
			end	
						
	      RX_IDLE:begin
			
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b1;
					 counter_enb_l			= 1'b1;
					 counter_sync_rst_l	= 1'b1;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b1;				
			
			end	
			
			default:begin
					 fifo_enb_l				= 1'b0;
					 arbiter_enb_l			= 1'b0;
					 write_l					= 1'b0;
					 clear_out_enb_l		= 1'b0;
					 counter_enb_l			= 1'b0;
					 counter_sync_rst_l	= 1'b0;
					 fifo_mask_l			= 1'b0;
					 length_o_l				= 1'b0;
					 source_l				= 1'b0;
					 destination_l			= 1'b0;
					 mask_send_l			= 1'b0;
			end
			
	endcase
	
end

assign fifo_enb			= fifo_enb_l;
assign arbiter_enb		= arbiter_enb_l;
assign write				= write_l;
assign clear_out_enb		= clear_out_enb_l;
assign counter_enb		= counter_enb_l;
assign counter_sync_rst	= counter_sync_rst_l;
assign fifo_mask			= fifo_mask_l;
assign length_o			= length_o_l;
assign source				= source_l;
assign destination		= destination_l;
assign mask_send			= mask_send_l;

endmodule
