`timescale 1ns / 1ps
module tbp1;
localparam DW = 8;
logic  [DW-1:0] multiplier;
logic  [DW-1:0] multiplicand;
logic pb_i;
logic rst;
logic clk;
logic ready;
logic [2*DW-1:0] result;



mult_sequential_FSM uut(
.multiplier(multiplier),
.multiplicand(multiplicand),
.pb_i(pb_i),
.rst(rst),
.clk(clk),
.ready(ready),
.result(result)
);


initial begin
            clk     = 0;
  	    rst     = 0;
	    pb_i    = 0;
	    result  = 0;
            multiplier   = -128;
	    multiplicand = 127;
    #4     rst     = 0;
    #1     rst     = 1;
    #2      
    #2	    pb_i    = 1;
    #2	    pb_i    = 0;
    #100
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule