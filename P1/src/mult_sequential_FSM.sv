//Author:  		Maria Luisa Ortiz Carstensen and Victor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Top Module
//Date:	  		13/02/2024

module mult_sequential_FSM
#(
parameter DW = 8 
)
(
	input  [DW-1:0] 	multiplier, // Multiplier input from switches
	input  [DW-1:0] 	multiplicand, // Multiplicand input from switches
	input bit 	 		 		pb_i, // Start button
	input bit 	 		 		rst,
	input bit 	 		 		clk,
	output 				ready, // Signal ready
   output [2*DW-1:0] 		result //Result in 16 bits to show in screens
);


// Wires to make connections between modules

wire [DW -1 : 0] mux_multiplierOut_w; 
wire [DW -1 : 0] pipo_MultiplierOut_w;
wire [DW -1 : 0] pipo_MultiplierOut_shifted_w;

wire [DW -1 : 0]   pipo_MultiplicandOut_w;
wire [2*DW -1 : 0] extendSign_Out_w;
wire [2*DW -1 : 0] mux_multiplicandOut_w;
wire [2*DW -1 : 0] rAddOut_w;
wire [2*DW -1 : 0] rSubOut_w;
wire [2*DW -1 : 0] AccOut_w;
wire [2*DW -1 : 0] mux_CounterOut_w;
wire [2*DW -1 : 0] pipo_Acc_shiftedOut_w;
wire [2*DW -1 : 0] resultOut_w;


wire  pulse_out_w;
wire  startSequence_out_w;
wire  sync_rst_acc_w;
wire  sync_rst_result_w;
wire  sync_rst_counter_W;
wire  sync_rst_W;
wire [3 : 0]counter_out_w;
wire  				index_w;
wire  				ready_w;
wire enable_gen_reg_w;
wire enable_prod_reg_w;

//Modules declaration and interconection

// Multiplexor to select the multlplirt and the multiplier shifted n times
mux_DW
#(
    .DW(DW)  // Data width
)
mux_multiplier
(
	 .sel(startSequence_out_w),           // Signal bit selection (N bits)
	 .dataA(multiplier),  				     // Multiplier Input (N x DATA_WIDTH bits)
	 .dataB(pipo_MultiplierOut_shifted_w),// Multiplier shifted as input
	 .dataOut (mux_multiplierOut_w)       // Output from mux
);

// Pipo register to receive the multipler data
pipo #(
	.DW(DW)
)
Multiplier
(
	 .clk(clk),
	 .rst(rst),
	 .enb(enable_gen_reg_w),
	 .sync_rst(sync_rst_W), 
    .inp(mux_multiplierOut_w),
    .out(pipo_MultiplierOut_w)
);

// Module to make a shift to the left one time from the multipler
left_rightShifter #(
   .DW(DW),
   .shifting(1)
)
leftTorightshif
 (
   .data_i(pipo_MultiplierOut_w),
   .shifted_data_o(pipo_MultiplierOut_shifted_w)
);

// Pipo register to receive the multipler data

pipo #(
	.DW(DW)
)
Multiplicand
(
	 .clk(clk),
	 .rst(rst),
	 .enb(enable_gen_reg_w), 
	 .sync_rst(sync_rst_W), 
    .inp(multiplicand),
    .out(pipo_MultiplicandOut_w)
);

// Module to adjust the bit depth of data, in this case allows to convert the multilicand and multiplier
// into a data of 16 bits insted of 8 bits. Aswell cares the sign of the numbers filling with 1 if it is
// negative or filling with ceros if it is positive.
extended_sign
#(
 	 .DW(8),
	 .E_bits(8)
)
extended_sign_Multiplicand
(
	 .data_i(pipo_MultiplicandOut_w),
	 .data_o(extendSign_Out_w)
);



mux_DW
#(
	.DW(2*DW)  // Data width
)
mux_multiplicand
(
  	.sel(pipo_MultiplierOut_w[DW-1]),   // Multiplier Output (N x DATA_WIDTH bits)
  	.dataA({(2*DW){1'b0}}),  				// Input addition (N x DATA_WIDTH bits)
  	.dataB(extendSign_Out_w),           // Input substraction (N x DATA_WIDTH bits)
   .dataOut (mux_multiplicandOut_w)       // Data output (DATA_WIDTH bits)
);

//Module that allows to substract or add numbers according to process of multiplication
sub_add_data 
#(
   .DW(2*DW) // Data width
)
sub_add_operation
 (
	.A_i(mux_multiplicandOut_w),     // Input from multiplicand
	.B_i(pipo_Acc_shiftedOut_w),     // Input from acumalator shifted
	.Sub_o(rSubOut_w), // Output substraction
	.Add_o(rAddOut_w) // Output substraction
);


mux_DW
#(
	.DW(2*DW)  // Data width
)
mux_counter
(
  	.sel(index_w),                   // Signal bit selection (N bits)
  	.dataA(rAddOut_w),  				   // Inputs from sub/add module (N x DATA_WIDTH bits)
  	.dataB(rSubOut_w),               // Inputs from sub/add module (N x DATA_WIDTH bits)
   .dataOut (mux_CounterOut_w)      // Output (Ancho de datos)
);

// Pipo register module to save the accumulator data

pipo 
#(
	.DW(2*DW)
)
ACC
(
	.clk(clk),
	.rst(rst),
	.enb(startSequence_out_w), 
	.sync_rst(sync_rst_acc_w), 
   .inp(mux_CounterOut_w),
   .out(AccOut_w)
);

// Module to make a shift to the left one time from the accumulator

left_rightShifter #(
	.DW(2*DW),
	.shifting(1)
)
leftTorightshifterAcc
 (
	.data_i(AccOut_w),
	.shifted_data_o(pipo_Acc_shiftedOut_w)
);

// Register to save the data when the procces is done

pipo 
#(
.DW(2*DW)
)
Result
(
	.clk(clk),
	.rst(rst),
	.enb(enable_prod_reg_w), 
	.sync_rst(sync_rst_result_w), 
   .inp(AccOut_w),
   .out(resultOut_w)
);

////////////////////////////////////////////////////////////////////////////////////////

//This module counts the clock cycles to allow the FSM to give the enable the right signal into the datapath

Counter
#(
   .DW(4)
)
counter
(
   .enable_i(startSequence_out_w),
   .clk_i(clk),
   .rst_i(rst),
   .sync_rst(sync_rst_counter_W),
   .Output_o(counter_out_w)
);

// One shot module to help having a input aligned with the clock

one_shot
pb_Start
(
	.rst(rst),
	.clk(clk),
	.pb_i(pb_i),
	.pulse_o(pulse_out_w)
);

//FSM module, state machine that control signals to enable work correctly the datapath

FSM_Machine
FSM_MM
(
	.clk(clk),
	.reset(rst),
	.one_shoot_impulse_i(pulse_out_w),
	.cont(counter_out_w),
	.syncreset_i(1'b0),
	.syncreset_o(sync_rst_W),
	.syncreset_res_o(sync_rst_result_w),
	.syncreset_cnt_o(sync_rst_counter_W),
	.syncreset_acc_o(sync_rst_acc_w),
	.start_o(startSequence_out_w),
	.enable_o(enable_gen_reg_w),
	.enable_prod_o(enable_prod_reg_w),
	.index_o(index_w),
	.ready_o(ready_w)
);


assign result = resultOut_w;
assign ready = ready_w;


endmodule
