//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Two's Complement
//Date:	  		13/02/2024

//
// This module receive a number in complement A2 and convert in to decimal to show it in a 7 segment display
//


module A2toSevensegment(
	input [15:0]number,
	
	output [6:0] ss_screen1,
	output [6:0] ss_screen2,
	output [6:0] ss_screen3,
	output [6:0] ss_screen4,
	output [6:0] ss_screen5,
	output [6:0] ss_screen6
);

logic [15:0] num_l;
logic sign;

logic [19:0]bcd_output1_l;
logic [19:0]bcd_output2_l;
logic [19:0]bcd_output3_l;
logic [19:0]bcd_output4_l;
logic [19:0]bcd_output5_l;
logic [19:0]bcd_output6_l;
logic [19:0]bcd_output7_l;
logic [19:0]bcd_output8_l;
logic [19:0]bcd_output9_l;
logic [19:0]bcd_output10_l;
logic [19:0]bcd_output11_l;
logic [19:0]bcd_output12_l;
logic [19:0]bcd_output13_l;
logic [19:0]bcd_output14_l;
logic [19:0]bcd_output15_l;
logic [19:0]bcd_output16_l;

logic [6:0]segment_1_l;
logic [6:0]segment_2_l;
logic [6:0]segment_3_l;
logic [6:0]segment_4_l;
logic [6:0]segment_5_l;
logic [6:0]segment_6_l;

bcd_step #(0) step0(.bin(num_l), .bcd_in(0), 				.bcd_out(bcd_output1_l));
bcd_step #(1) step1(.bin(num_l), .bcd_in(bcd_output1_l), .bcd_out(bcd_output2_l));
bcd_step #(2) step2(.bin(num_l), .bcd_in(bcd_output2_l), .bcd_out(bcd_output3_l));
bcd_step #(3) step3(.bin(num_l), .bcd_in(bcd_output3_l), .bcd_out(bcd_output4_l));
bcd_step #(4) step4(.bin(num_l), .bcd_in(bcd_output4_l), .bcd_out(bcd_output5_l));
bcd_step #(5) step5(.bin(num_l), .bcd_in(bcd_output5_l), .bcd_out(bcd_output6_l));
bcd_step #(6) step6(.bin(num_l), .bcd_in(bcd_output6_l), .bcd_out(bcd_output7_l));
bcd_step #(7) step7(.bin(num_l), .bcd_in(bcd_output7_l), .bcd_out(bcd_output8_l));
bcd_step #(8) step8(.bin(num_l), .bcd_in(bcd_output8_l), .bcd_out(bcd_output9_l));
bcd_step #(9) step9(.bin(num_l), .bcd_in(bcd_output9_l), .bcd_out(bcd_output10_l));
bcd_step #(10) step10(.bin(num_l), .bcd_in(bcd_output10_l), .bcd_out(bcd_output11_l));
bcd_step #(11) step11(.bin(num_l), .bcd_in(bcd_output11_l), .bcd_out(bcd_output12_l));
bcd_step #(12) step12(.bin(num_l), .bcd_in(bcd_output12_l), .bcd_out(bcd_output13_l));
bcd_step #(13) step13(.bin(num_l), .bcd_in(bcd_output13_l), .bcd_out(bcd_output14_l));
bcd_step #(14) step14(.bin(num_l), .bcd_in(bcd_output14_l), .bcd_out(bcd_output15_l));
bcd_step #(15) step15(.bin(num_l), .bcd_in(bcd_output15_l), .bcd_out(bcd_output16_l));

seven_segment ss1( .a(bcd_output16_l[3:0]),   .segments(segment_1_l) );
seven_segment ss2( .a(bcd_output16_l[7:4]),   .segments(segment_2_l) );
seven_segment ss3( .a(bcd_output16_l[11:8]),  .segments(segment_3_l) );
seven_segment ss4( .a(bcd_output16_l[15:12]), .segments(segment_4_l) );
seven_segment ss5( .a(bcd_output16_l[19:16]), .segments(segment_5_l) );
seven_segment_sign ss6( .a_i(sign),             .segments_o(segment_6_l) );

always_comb begin
	if(number[15]>0) begin
		sign = 1;
		num_l = (~number)+1;
	end
	else begin
		sign = 0;
		num_l = number;
	end
end

assign ss_screen1 = segment_1_l;
assign ss_screen2 = segment_2_l;
assign ss_screen3 = segment_3_l;
assign ss_screen4 = segment_4_l;
assign ss_screen5 = segment_5_l;
assign ss_screen6 = segment_6_l;

endmodule



