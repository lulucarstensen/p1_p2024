//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Extended Sign
//Date:	  		13/02/2024
//
// This module can extend the bith depth of signed number
//

module extended_sign
#(
parameter 			 	 DW = 8,
parameter 			 E_bits = 8
)
(
input [(DW - 1) : 0] data_i,
output [((DW - 1) + E_bits) : 0] data_o
);

logic [((DW-1) + E_bits): 0] Exten_l;

always_comb
	begin
		Exten_l = {{E_bits{data_i[DW - 1]}}, data_i};
	end

assign data_o = Exten_l;

endmodule
