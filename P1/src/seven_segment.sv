//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Seven Segments
//Date:	  		13/02/2024

// Module that converts a nubmer into the seven segments display

module seven_segment(
	input [3:0]a,
	output [6:0]segments
);

logic [6:0]output_l;

always @(*) begin
	case(a)
		4'b0000: output_l = 7'b1000000; // "0"  
		4'b0001: output_l = 7'b1111001; // "1" 
		4'b0010: output_l = 7'b0100100; // "2" 
		4'b0011: output_l = 7'b0110000; // "3" 
		4'b0100: output_l = 7'b0011001; // "4" 
		4'b0101: output_l = 7'b0010010; // "5" 
		4'b0110: output_l = 7'b0000010; // "6" 
		4'b0111: output_l = 7'b1111000; // "7" 
		4'b1000: output_l = 7'b0000000; // "8"  
		4'b1001: output_l = 7'b0011000; // "9" 
		default: output_l = 7'b1000000; // "0"
	endcase
end

assign segments = output_l;

endmodule
