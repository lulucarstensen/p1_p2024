//Author:  		Victor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Mux Data Width
//Date:	  		13/02/2024

// Module to receive one external signal and bring it aligned with system clock

module one_shot(
input rst,
input clk,
input pb_i,
output pulse_o
);

logic out1_l;
logic out2_l;
logic pulse_l;

register #(1)reg_1 (.clk(clk), .reset(rst), .sync_rst(1'b0), .DataIn(pb_i), .enable(1'b1), .DataOut(out1_l));
register #(1)reg_2 (.clk(clk), .reset(rst), .sync_rst(1'b0), .DataIn(out1_l), .enable(1'b1), .DataOut(out2_l));

always_comb begin

pulse_l = (~out1_l) & out2_l;

end

assign pulse_o = pulse_l;

endmodule
