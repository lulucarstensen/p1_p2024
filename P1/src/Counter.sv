//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Counter
//Date:	  		13/02/2024

//
// This modules counts the clock cycles giving as an output the current count
//

module Counter
#(
    parameter DW = 4
)
(
    input enable_i,
    input clk_i,
    input rst_i,
    input sync_rst,
    output [DW-1 : 0] Output_o
);
logic [DW-1 : 0] Output_l;

always_ff@(posedge clk_i or negedge rst_i)
begin
    if(!rst_i)
        Output_l <= {(DW-1){1'b0}};
    else
		begin
			if(enable_i)
			begin 
				if(sync_rst)
					Output_l <= {(DW-1){1'b0}};
				else 
					begin
						Output_l <= Output_l + 1'b1;
					end
			end     
		end
end

assign	Output_o = Output_l;

endmodule
