//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Seven Segment Sign
//Date:	  		13/02/2024

// Module to show in a seven segement the sign of a number


module seven_segment_sign
(
	input a_i,
	
	output [6:0]segments_o
);

logic [6:0]output_l;

always @(*) begin
	case(a_i)
		1'b0: output_l = 7'b1111111; // "+"  case positive number
		1'b1: output_l = 7'b0111111; // "-"  case negative number
		default: output_l = 7'b1111111; // default case: nothing
	endcase
end

assign segments_o = output_l;

endmodule
