//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		FSM
//Date:	  		13/02/2024

//This module is a 4 states FSM that allows to control signlas in the datapath of the multiplier
//The counter input helps to decide when enables, syncresets ans ready signals  should be triggered
//

module FSM_Machine(
	input clk,
	input reset,
	input one_shoot_impulse_i,
	input [3:0]cont,
	input syncreset_i,
	
	output syncreset_o,
	output syncreset_res_o,
	output syncreset_cnt_o,
	output syncreset_acc_o,
	output start_o,
	output enable_o,
	output enable_prod_o,
	output index_o,
	output ready_o
);

typedef enum logic[1:0]{
	STATE_1 = 2'b00,
	STATE_2 = 2'b01,
	STATE_3 = 2'b10,
	STATE_4 = 2'b11

} fsm_state_type;

fsm_state_type current_state;
fsm_state_type next_state;

logic start_o_l;
logic index_o_l;
logic syncreset_o_l;
logic syncreset_res_o_l;
logic syncreset_cnt_o_l;
logic syncreset_acc_o_l;
logic ready_o_l;
logic enable_o_l;
logic enable_prod_o_l;

// First combinational section

always_comb begin
	case(current_state)
	STATE_1: begin //IDLE 
		if(one_shoot_impulse_i == 1)
			next_state = STATE_2;
		else 
			next_state = STATE_1;
	end
	STATE_2:begin //ENABLE
		next_state = STATE_3;
	end
	STATE_3: begin //
		if(cont <= 6)
			next_state = STATE_3;
		else 
			next_state = STATE_4;
	end
	STATE_4:begin
		next_state = STATE_1;
	end
	default: begin
		next_state = STATE_1;
	end
	endcase
end
// Sequential section

always_ff @(posedge clk, negedge reset) begin

	if(!reset)
		current_state <= STATE_1;
	else if(syncreset_i == 1'b1) 
		current_state <= STATE_1;
	else
		current_state <= next_state;
end

// Second combinational section
always_comb begin

	case(current_state)
	STATE_1: begin
		start_o_l = 1'b0;
		index_o_l = 1'b0;
		syncreset_o_l = 1'b0;
		syncreset_res_o_l = 1'b0;
		syncreset_cnt_o_l = 1'b0;
		syncreset_acc_o_l = 1'b1;
		enable_o_l = 1'b1;
		enable_prod_o_l = 1'b0;
		ready_o_l = 1'b1;
	end
	STATE_2: begin
		start_o_l = 1'b1;
		index_o_l = (cont == 1'b0) ? 1'b1 : 1'b0;;
		syncreset_o_l = 1'b0;
		syncreset_res_o_l = 1'b0;
		syncreset_cnt_o_l = 1'b0;
		syncreset_acc_o_l = 1'b0;
		enable_o_l = 1'b1;
		enable_prod_o_l = 1'b0;
		ready_o_l = 1'b0;
	end
	STATE_3: begin
		start_o_l = 1'b1;
		index_o_l = 1'b0;
		syncreset_o_l = 1'b0;
		syncreset_res_o_l = 1'b0;
		syncreset_cnt_o_l = 1'b0;
		syncreset_acc_o_l = 1'b0;
		enable_o_l = 1'b1;
		enable_prod_o_l = 1'b0;
		ready_o_l = 1'b0;
	end
	STATE_4: begin
		start_o_l = 1'b1;
		index_o_l = 1'b0;
		syncreset_o_l = 1'b0;
		syncreset_res_o_l = 1'b0;
		syncreset_cnt_o_l = 1'b1;
		syncreset_acc_o_l = 1'b1;
		enable_o_l = 1'b0;
		enable_prod_o_l = 1'b1;
		ready_o_l = 1'b0;
	end	
	default: begin
		start_o_l = 1'b0;
		index_o_l = 1'b0;
		syncreset_o_l = 1'b1;
		syncreset_res_o_l = 1'b0;
		syncreset_cnt_o_l = 1'b1;
		syncreset_acc_o_l = 1'b1;
		enable_o_l = 1'b0;
		enable_prod_o_l = 1'b0;
		ready_o_l = 1'b0;
		end
	endcase
end

// Output assignment section
assign start_o = start_o_l;
assign index_o = index_o_l;
assign syncreset_o = syncreset_o_l;
assign syncreset_res_o = syncreset_res_o_l;
assign syncreset_cnt_o = syncreset_cnt_o_l;
assign syncreset_acc_o = syncreset_acc_o_l;
assign ready_o = ready_o_l;
assign enable_prod_o = enable_prod_o_l;
assign enable_o = enable_o_l;

endmodule
