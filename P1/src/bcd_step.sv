//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		BCD Step
//Date:	  		13/02/2024

//
// This module is part of the algorithm to convert binary data to decimal
//

module bcd_step #(parameter num = 0)(
input  [15 : 0] bin,
input  [19 : 0]bcd_in,
output [19 : 0]bcd_out
);

logic [19 : 0]bdc_l;

always_comb begin
	bdc_l = bcd_in;
   if (bdc_l[3 : 0] >= 4'b0101) 
		bdc_l[3 : 0] = bdc_l[3 : 0] + 2'b11;		
	if (bdc_l[7 : 4] >= 4'b0101) 
		bdc_l[7 : 4] = bdc_l[7 : 4] + 2'b11;
	if (bdc_l[11 : 8] >= 4'b0101) 
		bdc_l[11 : 8] = bdc_l[11 : 8] + 2'b11;
	if (bdc_l[15 : 12] >= 4'b0101) 
		bdc_l[15 : 12] = bdc_l[15 : 12] + 2'b11;
	if (bdc_l[19 : 16] >= 4'b0101) 
		bdc_l[19 : 16] = bdc_l[19 : 16] + 2'b11;
	bdc_l = {bdc_l[18 : 0],bin[15 - num]};
end

assign bcd_out = bdc_l;

endmodule
