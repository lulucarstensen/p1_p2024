//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Shifts
//Date:	  		13/02/2024
//
// This module shifts data to the left n times according to the input parameter 
//
//
//
module left_rightShifter #(
  parameter DW = 8,
  parameter shifting = 1
) (
  input wire [DW-1:0] data_i,
  output wire [DW-1:0] shifted_data_o
);

  logic [DW-1:0] sh_data_l;

  always_comb begin

      sh_data_l = data_i << shifting;
 
  end

  assign shifted_data_o = sh_data_l;

endmodule
