//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Subtraction and Addition 
//Date:	  		13/02/2024


// Module to allow make a substraction or addtion of two numbers

module sub_add_data 
#(
  parameter DW = 8 // Data Width
) (
  input  [DW-1:0] A_i,     
  input  [DW-1:0] B_i,     
  output  [DW-1:0] Sub_o, 
  output  [DW-1:0] Add_o 
);


assign Sub_o = B_i - A_i;  // Substraction
assign Add_o = B_i + A_i;  // Additionn


endmodule
