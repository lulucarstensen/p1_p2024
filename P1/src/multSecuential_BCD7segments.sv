//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Sequential Multiplication, BCD 7 Segments
//Date:	  		13/02/2024

module multSecuential_BCD7segments #(parameter DW = 8)
(
	input  [DW - 1 : 0] multiplier,
	input  [DW - 1 : 0] multiplicand,
	input	 	bit 	 		 	  pb_i,
	input  	bit 	 		 	   rst,
	input  	bit 	 		 	   clk,
	output	bit 				 ready,
   output [6 : 0] 	  ss_screen1,
   output [6 : 0] 	  ss_screen2,
   output [6 : 0] 	  ss_screen3,
   output [6 : 0] 	  ss_screen4,
   output [6 : 0] 	  ss_screen5,
   output [6 : 0] 	  ss_screen6
);
////////////////-WIREs-/////////////////
wire [2*DW-1 : 0] result_out_W;
wire [6 : 0]		ss_screen1_W;
wire [6 : 0]		ss_screen2_W;
wire [6 : 0]		ss_screen3_W;
wire [6 : 0]		ss_screen4_W;
wire [6 : 0]		ss_screen5_W;
wire [6 : 0]		ss_screen6_W;

wire clock_1MHz_wire;


///////////////////////////////////////

my_pll
OneMHzClock
(
	.areset(~rst),
	.inclk0(clk),
	.c0(clock_1MHz_wire),
	.locked()
);


mult_sequential_FSM
#(
 .DW(8) 
)
Multiplier
(
	.multiplier(multiplier),
	.multiplicand(multiplicand),
	.pb_i(pb_i),
	.rst(rst),
	.clk(clock_1MHz_wire),
	.ready(ready),
   .result(result_out_W)
);



A2toSevensegment
display
(
.number(result_out_W),
.ss_screen1(ss_screen1_W),
.ss_screen2(ss_screen2_W),
.ss_screen3(ss_screen3_W),
.ss_screen4(ss_screen4_W),
.ss_screen5(ss_screen5_W),
.ss_screen6(ss_screen6_W)
);

assign ss_screen1 = ss_screen1_W;
assign ss_screen2 = ss_screen2_W;
assign ss_screen3 = ss_screen3_W;
assign ss_screen4 = ss_screen4_W;
assign ss_screen5 = ss_screen5_W;
assign ss_screen6 = ss_screen6_W;


endmodule
