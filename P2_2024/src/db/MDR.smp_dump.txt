
State Machine - |UART_MDR|UART_ECHO:UART|UART_Rx:Rx_Top|FSM_UART:fsm|current_state
Name current_state.PARITY current_state.DATA current_state.START current_state.IDLE current_state.DONE 
current_state.IDLE 0 0 0 0 0 
current_state.START 0 0 1 1 0 
current_state.DATA 0 1 0 1 0 
current_state.PARITY 1 0 0 1 0 
current_state.DONE 0 0 0 1 1 

State Machine - |UART_MDR|UART_ECHO:UART|uart_tx:Tx_Top|FSM_Machine1:uart_fsm|current_state
Name current_state.STATE_IDLE current_state.STATE_DATA_TX current_state.STATE_FRAME_CONSTRUCTION 
current_state.STATE_IDLE 0 0 0 
current_state.STATE_FRAME_CONSTRUCTION 1 0 1 
current_state.STATE_DATA_TX 1 1 0 

State Machine - |UART_MDR|sqrt_root:MDR|FSM_Machine:FSM_mdr|current_state
Name current_state.STATE_SET_RESULT current_state.STATE_CALCULATE current_state.STATE_LOAD_Y current_state.STATE_LOAD_X current_state.STATE_START_SIGNAL current_state.STATE_IDLE 
current_state.STATE_IDLE 0 0 0 0 0 0 
current_state.STATE_START_SIGNAL 0 0 0 0 1 1 
current_state.STATE_LOAD_X 0 0 0 1 0 1 
current_state.STATE_LOAD_Y 0 0 1 0 0 1 
current_state.STATE_CALCULATE 0 1 0 0 0 1 
current_state.STATE_SET_RESULT 1 0 0 0 0 1 

State Machine - |UART_MDR|sqrt_root:MDR|FSM_Machine:FSM_mdr|current_operation
Name current_operation.NOT_SUPPORTED current_operation.SQRT_ROOT current_operation.DIVISION current_operation.MULTIPLICATION 
current_operation.MULTIPLICATION 0 0 0 0 
current_operation.DIVISION 0 0 1 1 
current_operation.SQRT_ROOT 0 1 0 1 
current_operation.NOT_SUPPORTED 1 0 0 1 
