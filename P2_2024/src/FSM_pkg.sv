// Project: MDR
// Module:  MDR FSM Pkg
// Author:  María Luisa Ortiz Carstensen
// Date:    April 14th, 2024 


package FSM_pkg;

// * Parameters * 
	
	localparam OP_CNT = 3;
	localparam SEL = 1;
	localparam RMDR = 9;
	
	localparam DW = 10;
	localparam DW_C = 4; // Data Width for Counter
	localparam DW_S = DW*2; // Data Width for Shifted Reg
	
	
//	typedef logic [OP_CNT:0]operation_count;
//	typedef logic [SEL:0] operation_sel_i;
//	typedef logic [RMDR:0]   reminder_i

// STATE 
typedef enum logic[2:0]{
	STATE_IDLE = 3'b000,
	STATE_START_SIGNAL = 3'b001,
	STATE_LOAD_X = 3'b010,
	STATE_LOAD_Y = 3'b011,
	STATE_CALCULATE = 3'b100,
	STATE_SET_RESULT = 3'b101
} fsm_state_type;

// OPERATION 
typedef enum logic[1:0]{
	MULTIPLICATION = 2'b00,
	DIVISION = 2'b01,
	SQRT_ROOT = 2'b10,
	NOT_SUPPORTED = 2'b11
} operation_type;

endpackage


