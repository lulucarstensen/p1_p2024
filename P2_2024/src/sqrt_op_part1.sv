module sqrt_op_part1
(
	input [DW - 1 : 0]   data_d,
	input [DW - 1 : 0]   data_q,
	input [DW - 1 : 0]   reminder_i,
	input [OP_CNT:0]   count,

	output [DW - 1 : 0] reminder_o,
	output [DW - 1 : 0] data_to_add_sub,
	output sub_add_selector_sqrt_op_p2
);

logic [DW - 1 : 0] partial_reminder_l;
logic [DW - 1 : 0] data_to_add_sub_l;
logic sub_add_selector;
always_comb begin
	if (reminder_i <= 10'b0111111111) begin
		partial_reminder_l = ((data_d >> ((4'b1000-count)+(4'b1000-count))) & 2'b11) | (reminder_i << 2'b10);
		data_to_add_sub_l = (data_q<< 2) | 1'b1;
		sub_add_selector = 1'b1;
		end
	else begin
		partial_reminder_l = ((data_d >> ((4'b1000-count)+(4'b1000-count))) & 2'b11) | (reminder_i << 2'b10);
		data_to_add_sub_l = (data_q<< 2) | 2'b11;
		sub_add_selector = 1'b0;
		end

end

assign reminder_o = partial_reminder_l;
assign data_to_add_sub = data_to_add_sub_l;
assign sub_add_selector_sqrt_op_p2 = sub_add_selector;

endmodule