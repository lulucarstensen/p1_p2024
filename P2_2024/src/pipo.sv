//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		Sequential Multiplier
//Module:  		PIPO Register 
//Date:	  		13/02/2024

//Module to create a PIPO register according to paramater DW equals Data Width


module pipo #(parameter DW = 8) 
(
input  bit              clk,
input  bit              rst,
input  logic            enb,
input  logic 			 	sync_rst, 
input  logic [DW-1:0]   inp,

output logic [DW-1:0]   out
);

logic [DW-1:0]      rgstr_r;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else 
		if (enb)
			begin
				if(sync_rst)
					rgstr_r <= '0;
				else
					rgstr_r  <= inp;
			end
end:rgstr_label

assign out = rgstr_r;

endmodule
