module sqrt_op_part2
(
	input [DW - 1 : 0]   data_addition,
	input [DW - 1 : 0]   data_substraction,
	input 	  	  sub_add_selector_i,
	input [DW - 1 : 0]   data_q_i,

	output [DW - 1 : 0] reminder_o,
	output [DW - 1 : 0] data_q_o,
	output debug_flag
);

logic [DW - 1 : 0] temp_reminder_l;
logic [DW - 1 : 0] temp_q;
logic flag;

always_comb begin
	if(sub_add_selector_i == 1'b1) begin
		temp_reminder_l = data_substraction;
		if(data_substraction <= 10'b0111111111) begin
			temp_q = ( data_q_i << 1 ) | 1'b1;
			flag = 1'b1;
		end	
		else begin
			temp_q = ( data_q_i << 1 ) | 1'b0;
			flag = 1'b0;
		end
	end
	else begin
		temp_reminder_l = data_addition;
		if(data_addition <= 10'b0111111111) begin
			temp_q = ( data_q_i << 1 ) | 1'b1;
			flag = 1'b1;
			end
		else begin
			temp_q = ( data_q_i << 1 ) | 1'b0;
			flag = 1'b0;
			end
	end
end

assign reminder_o = temp_reminder_l;
assign data_q_o = temp_q;

endmodule