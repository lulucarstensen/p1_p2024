//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		MDR
//Module:  		Shifted PIPO
//Date:	  		13/02/2024


import FSM_pkg::*;

module shifted_pipo 
(
input  bit              clk,
input  bit              rst,
input  logic            enb,
input  logic 			 	sync_rst, 
input  logic [DW_S:0]   inp,

output logic [DW_S:0]   out
);

logic [DW_S:0]      rgstr_r;

always_ff@(posedge clk or negedge rst) begin
    if(!rst)
        rgstr_r  <= '0;
    else 
		if (enb)
			begin
				if(sync_rst)
					rgstr_r <= '0;
				else
					rgstr_r  <= inp;
			end
end

assign out = rgstr_r;

endmodule
