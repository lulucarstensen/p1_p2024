

import FSM_pkg::*;
module sqrt_root
(
input  clk_50MHz,
input  rst,
input start,
input load,
input [DW - 1 : 0]   data_in,
input[SEL:0] operation_sel,

output [DW - 1 : 0] result,
output [DW - 1 : 0] remainder,
output ready,
output load_X,
output load_Y,
output error
);

wire enb_pipo_data_x;
wire [DW - 1 : 0] pipo_data_x_out;
wire enb_pipo_data_y;
wire [DW - 1 : 0] pipo_data_y_out;
wire enb_pipo_result;
wire [DW - 1 : 0] pipo_result_out;
wire enb_pipo_reminder;
wire [DW - 1 : 0] pipo_remainder_out;
wire enb_pipo_accumulator_rem;
wire [DW - 1 : 0] pipo_accumulator_rem_out;
wire enb_pipo_x_mult_w;
wire [DW - 1 : 0] pipo_x_mult;

wire enb_operation_counter;
wire [OP_CNT:0] operation_counter;

wire sel_mux_operation_result;
wire [DW - 1 : 0] mux_operation_result_out;
wire sel_mux_input_prev_x;
wire [DW - 1 : 0] mux_data_prev_x_out;
wire sel_mux_input_data_x;
wire [DW - 1 : 0] mux_data_x_out;
wire sel_mux_input_data_y;
wire [DW - 1 : 0] mux_data_y_out;


wire [DW - 1 : 0] sqrt_part1_out_data1;
wire [DW - 1 : 0] sqrt_part1_out_data2;
wire [DW - 1 : 0] substraction_to_sqrt_op_part2;
wire [DW - 1 : 0] addition_to_sqrt_op_part2;
wire [DW - 1 : 0] sqrt_op_part2_remainder_out;
wire [DW - 1 : 0] sqrt_op_part2_dataq_out;

wire sync_rst_w;
wire ready_signal_w;
wire load_x_signal_w;
wire load_y_signal_w;
wire sub_add_selector_sqrt_p2_w;
wire enb_booth_w;
wire flag_calculate_state_w;

// W I R E S

wire ovf_w;

wire [DW - 1 : 0] data_w;
wire [DW - 1 : 0] X_w; // M
wire [DW - 1 : 0] Y_w;
wire [DW - 1 : 0]M_mux_w; // 3 to 1 mux output
wire [DW - 1 : 0]add_result_w;// Addition
//wire [DW - 1 : 0]add_pipo_w;// Addition
wire [DW - 1 : 0]shift_count_w;
wire [DW - 1 : 0]counter_for_data_w;

wire [(DW*2) : 0]prod_conc_o_w;
wire [(DW*2) : 0]shifted_w;
wire [(DW*2) : 0]shifted_reg_w;
wire [(DW*2) : 0]product_w;
wire [(DW*2) : 0]reg_to_shift_w;

wire clk_w;
wire clk_10Mhz_w;

PLL_MDR 
(
	.areset(~rst),
	.inclk0(clk_50MHz),
	.c0(clk_10Mhz_w),
	.locked(/*Disconnected*/)
);


//booth mult
Booth_Concatenate		 Concatenation
(
	.Q_i(pipo_data_y_out),
	
	.product_o(prod_conc_o_w)
);

mux_3to1 	mux3_1
(	
  .Q(reg_to_shift_w[1]),			// Selector [1] 
  .Qminus(reg_to_shift_w[0]), 	// Selector [0]
  .M(pipo_x_mult),			// Multiplicand
  
  .booth_op(M_mux_w)	// M version or Zero
);

sub_add_data
#(
   .DW(10)
)	 
booth_add
(
  .A_i(reg_to_shift_w[(DW*2) : (DW + 1)]), // Accumulator     
  .B_i(M_mux_w), // M version  
  
  .Sub_o( ), 
  .Add_o(add_result_w) 
);

Shift_Right 	booth_shift
(
	.clk(clk_10Mhz_w),
	.rst(rst),
	.enb(enb_booth_w),
	.sync_rst(sync_rst_w), 
	.shift_i({add_result_w,reg_to_shift_w[DW:0]}),//[(DW*2):0]->[DW-1]:0+[DW:0]->[1:0]+[2:0]->[2,3],
	.flag_calculate_state(flag_calculate_state_w),
	
	.shift_o(shifted_w),
	.shift_ovf(ovf_w),
	.shift_count(shift_count_w)
);

//shifted_pipo	Shifted 
//(
//	.clk(clk),
//	.rst(rst),
//	.enb(enb_booth_w),
//	.sync_rst(sync_rst_w), 
//	.inp(shifted_w),
//
//	.out(shifted_reg_w)
//);

// Determines entry for Operation
Counter_Mux counter_shift_mux 
(
    .shift_count(shift_count_w),
    .shift_reg(shifted_w),
    .conc_reg(prod_conc_o_w),// initial value 00 Q 0 
	 
    .counter_mux_o(reg_to_shift_w)
);	

pipo #(
	.DW(10)
)
pipo_data_x_mult
(
	 .clk(clk_10Mhz_w),
	 .rst(rst),
	 .enb(enb_pipo_x_mult_w), 
	 .sync_rst(sync_rst_w), 
    .inp(data_in),
    .out(pipo_x_mult)
);

//mux_product	 mux_prod
//(
//  .sel(ovf_w),	// Shift ovf
//  .dataA(0),	// Zero 
//  .dataB(shifted_w),   // Shifted value
//  
//  .product(product_w)	// Output (Data Width)
//);	


// sqrt root

mux_DW
#(
    .DW(10)  // Data width
)
mux_data_prev_x
(
	 .sel(sel_mux_input_prev_x),        
	 .dataA(10'b0000000000),  				   
	 .dataB(data_in), //output from sqrt op part 2
	 .dataOut (mux_data_prev_x_out)     
);

mux_DW
#(
    .DW(10)  // Data width
)
mux_datax
(
	 .sel(sel_mux_input_data_x),        
	 .dataA(mux_data_prev_x_out),  				   
	 .dataB(sqrt_op_part2_dataq_out), //output from sqrt op part 2
	 .dataOut (mux_data_x_out)     
);

pipo #(
	.DW(10)
)
pipo_data_x
(
	 .clk(clk_10Mhz_w),
	 .rst(rst),
	 .enb(enb_pipo_data_x), 
	 .sync_rst(sync_rst_w), 
    .inp(mux_data_x_out),
    .out(pipo_data_x_out)
);

mux_DW
#(
    .DW(10)  // Data width
)
mux_data_y
(
	 .sel(sel_mux_input_data_y),        
	 .dataA(data_in),  				   
	 .dataB(pipo_accumulator_rem_out),
	 .dataOut (mux_data_y_out)     
);

pipo #(
	.DW(10)
)
pipo_data_y
(
	 .clk(clk_10Mhz_w),
	 .rst(rst),
	 .enb(enb_pipo_data_y), 
	 .sync_rst(sync_rst_w), 
    .inp(mux_data_y_out),
    .out(pipo_data_y_out)
);

pipo #(
	.DW(10)
)
pipo_remainder
(
	 .clk(clk_10Mhz_w),
	 .rst(rst),
	 .enb(enb_pipo_reminder), 
	 .sync_rst(sync_rst_w), 
    .inp(pipo_accumulator_rem_out), // from sqrt op part 3
    .out(pipo_remainder_out)
);

pipo #(
	.DW(10)
)
pipo_accumulator_remainder
(
	 .clk(clk_10Mhz_w),
	 .rst(rst),
	 .enb(enb_pipo_accumulator_rem), 
	 .sync_rst(sync_rst_w), 
    .inp(sqrt_op_part2_remainder_out), // from sqrt op part 3
    .out(pipo_accumulator_rem_out)
);

mux_DW
#(
    .DW(10)  // Data width
)
mux_operation_result
(
	 .sel(sel_mux_operation_result),        
	 .dataA(sqrt_op_part2_dataq_out),  				   
	 .dataB(shifted_w),
	 .dataOut (mux_operation_result_out)     
);

pipo #(
	.DW(10)
)
pipo_result
(
	 .clk(clk_10Mhz_w),
	 .rst(rst),
	 .enb(enb_pipo_result), 
	 .sync_rst(sync_rst_w), 
    .inp(mux_operation_result_out), // from sqrt op part 2
    .out(pipo_result_out)
);

Counter2
#(
   .DW(4),
	.OV(10)
)
operation_counter_op
(
   .enable_i(enb_operation_counter),
   .clk_i(clk_10Mhz_w),
   .rst_i(rst),
   .sync_rst(sync_rst_w),
   .Output_o(operation_counter),
	.ovflw()
);

sqrt_op_part1
sqrt_part_1
(
	.data_d(pipo_data_y_out),
	.data_q(pipo_data_x_out),
	.reminder_i(pipo_accumulator_rem_out),
	.count(operation_counter),
	.reminder_o(sqrt_part1_out_data1),
	.data_to_add_sub(sqrt_part1_out_data2),
	.sub_add_selector_sqrt_op_p2(sub_add_selector_sqrt_p2_w)
);

sub_add_data 
#(
   .DW(10)
)
sub_add_module
 (
	.A_i(sqrt_part1_out_data2),
	.B_i(sqrt_part1_out_data1),
	.Sub_o(substraction_to_sqrt_op_part2),
	.Add_o(addition_to_sqrt_op_part2)
);

sqrt_op_part2
sqrt_part_2
(
	.data_addition(addition_to_sqrt_op_part2),
	.data_substraction(substraction_to_sqrt_op_part2),
	.sub_add_selector_i(sub_add_selector_sqrt_p2_w),
	.data_q_i(pipo_data_x_out),

	.reminder_o(sqrt_op_part2_remainder_out), 
	.data_q_o(sqrt_op_part2_dataq_out)
);

FSM_Machine
FSM_mdr
(
	.clk(clk_10Mhz_w),
	.reset(rst),
	.syncreset_i(1'b0),
	.operation_count(operation_counter),
	.start_signal(start),
	.operation_sel_i(operation_sel),
	.reminder_i(pipo_accumulator_rem_out),
	
	.enb_pipo_data_x(enb_pipo_data_x),
	.enb_pipo_data_y(enb_pipo_data_y),
	.enb_pipo_result(enb_pipo_result),
	.enb_pipo_reminder(enb_pipo_reminder),
	.enb_pipo_accumulator_rem(enb_pipo_accumulator_rem),
	.enb_operation_counter(enb_operation_counter),
	.sel_mux_input_data_x(sel_mux_input_data_x),
	.sel_mux_input_data_y(sel_mux_input_data_y),
	.sync_rst(sync_rst_w),
	.ready_signal(ready_signal_w),
	.load_x_signal(load_x_signal_w),
	.load_y_signal(load_y_signal_w),
	.sel_mux_prev_x(sel_mux_input_prev_x),
	.sel_mux_operation_res(sel_mux_operation_result),
	.enb_booth_o(enb_booth_w),
	.flag_calculate_state(flag_calculate_state_w),
	.enb_pipo_x_mult(enb_pipo_x_mult_w)
);


assign result = pipo_result_out;
assign remainder = pipo_remainder_out;
assign ready = ready_signal_w;
assign load_X = load_x_signal_w;
assign load_Y = load_y_signal_w;
assign error = 1'b0;

endmodule

