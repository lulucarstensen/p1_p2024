// Project: MDR
// Module:  Booth Concatenate, It's the staring point
// Author:  María Luisa Ortiz Carstensen
// Date:    April 14th, 2024

import FSM_pkg::*;

module Booth_Concatenate
(	
	input [DW - 1: 0]Q_i, // Y
	
	output [(DW*2) : 0]product_o
);

assign product_o = {{DW{1'b0}}, Q_i, 1'b0};

endmodule
