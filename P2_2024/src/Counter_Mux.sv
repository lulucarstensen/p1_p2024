	// Project: MDR
// Module:  Counter Mux, for Booth Shifting
// Author:  María Luisa Ortiz Carstensen
// Date:    April 14th, 2024 

import FSM_pkg::*;

module Counter_Mux (
    input [DW-1:0] shift_count,
    input [DW_S:0] shift_reg,
    input [DW_S:0] conc_reg,
    output[DW_S:0] counter_mux_o
);

logic [DW_S:0]out_l;

always_comb begin
    if (shift_count != 0 && shift_count <= DW) begin
        out_l = shift_reg;
    end else begin
        out_l = conc_reg;
    end
end

assign counter_mux_o = out_l;

endmodule