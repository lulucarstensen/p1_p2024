//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		Counter
//Date:	  		13/02/2024

//
// This modules counts the clock cycles giving as an output the current count
//

module Counter2
#(
    parameter DW = 4,
    parameter OV = 4
)
(
    input enable_i,
    input clk_i,
    input rst_i,
    input sync_rst,
    output [DW-1 : 0] Output_o,
    output ovflw
);

logic [DW-1 : 0] Output_l;
logic ovf_l;

always_ff@(posedge clk_i or negedge rst_i)
begin
   if(!rst_i) begin
       Output_l <= {(DW-1){1'b0}};
       ovf_l <= 1'b0;
    end
   else if(sync_rst) begin 
        Output_l <= {(DW-1){1'b0}};
        ovf_l <= 1'b0;
    end
    else if (enable_i) begin 
        if (Output_l == OV) begin
          ovf_l  <= 1'b1;
          Output_l <= {DW{1'b0}};
        end
        else begin
          ovf_l  <= 1'b0;
          Output_l <= Output_l + 1'b1;
        end
    end
end

assign   Output_o = Output_l;
assign   ovflw = ovf_l;

endmodule
