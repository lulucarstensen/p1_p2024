import serial
import serial.tools.list_ports
import time

try:
    # Get available serial ports
    ports = serial.tools.list_ports.comports()
    portsList = [str(onePort) for onePort in ports]

    # Select a serial port
    print("Available ports:")
    for port in portsList:
        print(port)
    val = input("Select Port: COM")

    portVar = None
    for port in portsList:
        if port.startswith("COM" + str(val)):
            portVar = "COM" + str(val)
            print("Selected port:", portVar)
            break

    if portVar is None:
        raise ValueError("Invalid port selection")

    # Open serial connection
    serialInst = serial.Serial(portVar, baudrate=19200)

    # Print RX buffer before transmission
    print("RX Buffer before transmission:", serialInst.read(serialInst.in_waiting))

    # Define the data byte to be sent
    data_byte = 11 

    # Send the data byte
    print("Sending data byte:", data_byte)
    serialInst.write(bytes([data_byte]))

    # Wait for a short time to ensure transmission
    time.sleep(1)

    # Read the received data byte
    received_byte = serialInst.read()

    # Compare sent and received bytes
    if ord(received_byte) == data_byte:
        print("Sent and received data bytes match:", received_byte)
    else:
        print("Sent and received data bytes do not match:")
        print("Sent:", data_byte)
        print("Received:", received_byte)
        # Print RX buffer after transmission
        print("Rx Buffer after transmission:", serialInst.read(serialInst.in_waiting))

    # Close serial connection
    serialInst.close()

except Exception as e:
    print("An error occurred:", str(e))
