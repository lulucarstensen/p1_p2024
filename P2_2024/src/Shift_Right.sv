// Project: MDR
// Module: Shift Right
// Author: María Luisa Ortiz Carstensen
// Date: April 14th, 2024


import FSM_pkg::*;	

module Shift_Right
(
    input      clk,
    input      rst,
    input      enb,
    input      sync_rst,
    input [(DW*2) : 0] shift_i,
	 input flag_calculate_state,
	 
    output [(DW*2) : 0] shift_o,
    output     shift_ovf,
    output [DW - 1: 0] shift_count
);


logic [(DW*2):0] shifted_r; // [Acc:Q:Qminus]
logic [DW - 1: 0] shift_count_l;
logic ovf_l;

always_ff @ (posedge clk or negedge rst) begin : shifted_reg
    if (!rst) begin // Changed to active-high reset
        ovf_l <= 0;
        shift_count_l <= 0;
		  shifted_r <= 0;
    end else if (enb && flag_calculate_state) begin
        // Shift right and repeat MSB
        shifted_r <= {shift_i[(DW*2)], shift_i[(DW*2):1]};
				
        // Increment shift count only if shift operation occurs
        shift_count_l <= shift_count_l + 1;
            if (shift_count_l == DW) begin
                ovf_l <= 1;
                shift_count_l <= 0;
            end else begin
                ovf_l <= 0;
            end
        
    end
end : shifted_reg

assign shift_count = shift_count_l;
assign shift_o  = shifted_r;
assign shift_ovf = ovf_l;

endmodule
