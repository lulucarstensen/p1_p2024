// Project: MDR
// Module:  UART
// Author:  María Luisa Ortiz Carstensen
// Date:    April 14th, 2024 

import FSM_pkg::*;

module UART_MDR 
#(
   parameter DW = 8 
//   parameter PARITY = 2'b01, 
//   parameter BAUDRATE = 19200, 
//   parameter CLK_HZ = 10000000,
//   parameter SIMULATION = 0
) 
(
	input clk_50MHz,
	input rst,
	input rx,		// Rx Data
	input enb,
//	input RTS,		// Request to Send
	
	output tx,	// Tx Data 
	output tx_ready
//	output CTS // Clear to Send
);

//Wires
wire clk_10Mhz_w;
wire [7 : 0]uart_rx_data_w;
wire uart_rx_received_w;
wire [7 : 0]uart_tx_data_w;
wire uart_tx_ready_w;
wire uart_tx_send_w;
wire [1:0] op_select_w;
wire [9 : 0] mdr_data_w;
wire [9 : 0] mdr_result_w;
wire [9 : 0] mdr_reminder_w;

wire start_w;
wire load_w;
wire ready_w;
wire error_w;
wire load_x_w;
wire load_y_w;


PLL_MDR PLL_MDR
(
	.areset(~rst),
	.inclk0(clk),
	.c0(clk_10Mhz_w),
	.locked(/*Disconnected*/)
);

sqrt_root 
	MDR
(
	
	.clk_50MHz(clk_10Mhz_w),
	.rst(rst),
	.start(start_w),
	.load(load_w),
	.data_in(mdr_data_w),
	.operation_sel(op_select_w),

	.result(mdr_result_w),
	.remainder(mdr_reminder_w),
	.ready(ready_w),
	.load_X(load_x_w),
	.load_Y(load_y_w),
	.error(error_w)
	
);

UART_ECHO #(
	.DW(DW)
//	.PARITY(0), //2'b00 = none, 2'b01 = odd, 2'b10 = even, 2'b11 = unused
//   .SIMULATION(SIMULATION),
//	.BAUDRATE(BAUDRATE),
//   .CLK_HZ(CLK_HZ)
) UART
(
	
	.clk_50MHz(clk_10Mhz_w),
	.rst(rst),
	.enb(1'b1),
	.rx(uart_rx_data_w),		// Rx Data
//	input RTS,		// Request to Send
	
	.tx(uart_tx_data_w),	// Tx Data 
	.tx_ready(uart_tx_ready_w)
	
);

mdr_driver_10bits #(
	//Parameters with default values for perfect UART features developed
	.UART_TX_SWAP(1),
	.UART_RX_SWAP(0),
	.UART_RX_RECEIVED_ADD_1_SHOT(1)
) mdr_driver_10bits
(
	//System general
	.clk_10MHz(clk_10Mhz_w),
	.rst(rst),
	//UART ports
	//RX
	.uart_rx_data(uart_rx_data_w),
	.uart_rx_received(),
	//TX
   .uart_tx_ready(uart_tx_ready_w),
	.uart_tx_data(uart_tx_data_w),
	.uart_tx_send(),
	//MDR ports.
	//MDR inputs
	.mdr_data(mdr_data_w),
	.mdr_op(op_select_w),
	.mdr_start(start_w),
	.mdr_load(load_w),
	//MDR outputs
	.mdr_result(mdr_result_w),
	.mdr_reminder(mdr_reminder_w),
	.mdr_error(error_w),
	.mdr_ready(ready_w),
	.mdr_loadx(load_x_w),
	.mdr_loady(load_y_w)
);

endmodule
