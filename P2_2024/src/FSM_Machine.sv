//Author:  		Víctor Cervantes
//Project: 		Sequential Multiplier
//Module:  		FSM
//Date:	  		15/03/2024

import FSM_pkg::*;

module FSM_Machine(
	input clk,
	input reset,
	input syncreset_i,
	input [OP_CNT:0]operation_count,
	input start_signal,
	input[SEL:0] operation_sel_i,
	input [DW - 1 : 0] reminder_i,
	
	output enb_pipo_data_x,
	output enb_pipo_data_y,
	output enb_pipo_result,
	output enb_pipo_reminder,
	output enb_pipo_accumulator_rem,
	output enb_operation_counter,
	output sel_mux_input_data_x,
	output sel_mux_input_data_y,
	output sync_rst,
	output ready_signal,
	output load_x_signal,
	output load_y_signal,
	output sel_mux_prev_x,
	output sel_mux_operation_res,
	output enb_booth_o,
	output flag_calculate_state,
	output enb_pipo_x_mult
);

fsm_state_type current_state;
fsm_state_type next_state; 
operation_type current_operation;
operation_type next_operation;


logic enb_pipo_data_x_l;
logic enb_pipo_data_y_l;
logic enb_pipo_result_l;
logic enb_pipo_reminder_l;
logic enb_pipo_accumulator_rem_l;
logic enb_operation_counter_l;
logic sel_mux_input_data_x_l;
logic sel_mux_input_data_y_l;
logic sync_rst_l;
logic ready_sig_l;
logic load_x_sig_l;
logic load_y_sig_l;
logic sel_mux_prev_x_l;
logic sel_mux_operation_res_l;
logic enb_booth_l;
logic flag_calculate_state_l;
logic enb_pipo_x_mul_l;

// First combinational section

always_comb begin
    case(current_state)
        STATE_IDLE: begin
            if (start_signal == 1'b1) begin
                next_state = STATE_START_SIGNAL;
					 next_operation = current_operation;
            end
            else begin
                next_state = STATE_IDLE;
					 next_operation = current_operation;
            end
				
        end
        STATE_START_SIGNAL: begin
            next_state = STATE_LOAD_X;
				if (operation_sel_i == 2'b00)
				  next_operation = MULTIPLICATION;
				else if (operation_sel_i == 2'b01)
				  next_operation = DIVISION;
				else if (operation_sel_i == 2'b10)
				  next_operation = SQRT_ROOT;
				else
				  next_operation = NOT_SUPPORTED;
        end
        STATE_LOAD_X: begin
            next_state = STATE_LOAD_Y;
				next_operation = current_operation;
        end
        STATE_LOAD_Y: begin
            next_state = STATE_CALCULATE;
				next_operation = current_operation;
        end
        STATE_CALCULATE: begin
		  
				if(current_operation == SQRT_ROOT) begin
					if (operation_count == 4'b0111)
						 next_state = STATE_SET_RESULT;
					else
						next_state = STATE_CALCULATE;
					end
				else begin
					if (operation_count == 4'b1010)
						 next_state = STATE_SET_RESULT;
					else
						next_state = STATE_CALCULATE;			
					end
					
					
				next_operation = current_operation;
				
//				if (operation_count == 4'b1000)
//                next_state = STATE_SET_RESULT;
//				else
//					next_state = STATE_CALCULATE;
//				next_operation = current_operation;
				
        end
        STATE_SET_RESULT: begin
            next_state = STATE_IDLE;
				next_operation = current_operation;
        end
        default: begin
            next_state = STATE_IDLE;
				next_operation = MULTIPLICATION;
        end
    endcase
end

// Sequential section

always_ff @(posedge clk, negedge reset) begin

	if(!reset) begin
		current_state <= STATE_IDLE;
		current_operation = MULTIPLICATION;
	end
	else if(syncreset_i == 1'b1) begin
		current_state <= STATE_IDLE;
		current_operation = MULTIPLICATION;
	end
	else begin
		current_state <= next_state;
		current_operation <= next_operation;
	end
end

// Second combinational section
always_comb begin

	case(current_state)
	
	STATE_IDLE: begin
		enb_pipo_data_x_l = 1'b0;
		enb_pipo_data_y_l = 1'b0;
		enb_pipo_result_l = 1'b0; 
		enb_pipo_reminder_l = 1'b0;
		enb_pipo_accumulator_rem_l = 1'b0;
		enb_operation_counter_l = 1'b0;
		sel_mux_prev_x_l = 1'b0;
		sel_mux_input_data_x_l = 1'b0;
		sel_mux_input_data_y_l = 1'b1;
		sel_mux_operation_res_l = 1'b0;
		sync_rst_l = 1'b1;
		ready_sig_l = 1'b0;
		load_x_sig_l = 1'b0;
		load_y_sig_l = 1'b0;
		enb_booth_l = 1'b0;
		flag_calculate_state_l = 1'b0;
		enb_pipo_x_mul_l = 1'b0;
		end

	STATE_START_SIGNAL: begin
		enb_pipo_data_x_l = 1'b1;
		enb_pipo_data_y_l = 1'b0;
		enb_pipo_result_l = 1'b0; 
		enb_pipo_reminder_l = 1'b0;
		enb_pipo_accumulator_rem_l = 1'b0;
		enb_operation_counter_l = 1'b0;
		sel_mux_prev_x_l = 1'b0;
		sel_mux_input_data_x_l = 1'b0;
		sel_mux_input_data_y_l = 1'b1;
		sel_mux_operation_res_l = 1'b0;
		sync_rst_l = 1'b0;
		ready_sig_l = 1'b0;
		load_x_sig_l = 1'b0;
		load_y_sig_l = 1'b0;
		enb_booth_l = 1'b0;
		flag_calculate_state_l = 1'b0;
		enb_pipo_x_mul_l = 1'b1;
		end
	
	STATE_LOAD_X: begin
		enb_pipo_data_x_l = 1'b1;
		enb_pipo_data_y_l = 1'b0;
		enb_pipo_result_l = 1'b0; 
		enb_pipo_reminder_l = 1'b0;
		enb_pipo_accumulator_rem_l = 1'b0;
		enb_operation_counter_l = 1'b0;
		sel_mux_prev_x_l = (current_operation != SQRT_ROOT) ? 1'b1 : 1'b0;
		sel_mux_input_data_x_l = 1'b0;
//		sel_mux_input_data_x_l = (current_operation == SQRT_ROOT) ? 1'b0 : 1'b1;
		sel_mux_input_data_y_l = 1'b1;
		sel_mux_operation_res_l = 1'b0;
		sync_rst_l = 1'b0;
		ready_sig_l = 1'b0;
		load_x_sig_l = 1'b1;
		load_y_sig_l = 1'b0;
		enb_booth_l = 1'b1;
		flag_calculate_state_l = 1'b0;
		enb_pipo_x_mul_l = 1'b0;
		end
	
	STATE_LOAD_Y: begin
		enb_pipo_data_x_l = 1'b0;
		enb_pipo_data_y_l = 1'b1;
		enb_pipo_result_l = 1'b0; 
		enb_pipo_reminder_l = 1'b0;
		enb_pipo_accumulator_rem_l = 1'b0;
		enb_operation_counter_l = 1'b0;
		sel_mux_prev_x_l = 1'b0;
		sel_mux_input_data_x_l = 1'b0;
		sel_mux_input_data_y_l = 1'b0;
		sel_mux_operation_res_l = 1'b0;
		sync_rst_l = 1'b0;
		ready_sig_l = 1'b0;
		load_x_sig_l = 1'b0;
		load_y_sig_l = 1'b1;
		enb_booth_l = 1'b1;
		flag_calculate_state_l = 1'b0;
		enb_pipo_x_mul_l = 1'b0;
		end

	STATE_CALCULATE: begin
	
		case(current_operation)
		
			MULTIPLICATION: begin 
				enb_pipo_data_x_l = 1'b0;
				enb_pipo_data_y_l = 1'b0;
				enb_pipo_result_l = 1'b0; 
				enb_pipo_reminder_l = 1'b0;
				enb_pipo_accumulator_rem_l = 1'b1;
				enb_operation_counter_l = 1'b1;
				sel_mux_prev_x_l = 1'b0;
				sel_mux_input_data_x_l = 1'b1;
				sel_mux_input_data_y_l = 1'b1;
				sel_mux_operation_res_l = 1'b1;
				sync_rst_l = 1'b0;
				ready_sig_l = 1'b0;
				load_x_sig_l = 1'b0;
				load_y_sig_l = 1'b0;
				enb_booth_l = 1'b1;
				flag_calculate_state_l = 1'b1;
				enb_pipo_x_mul_l = 1'b0;
				end
			DIVISION: begin
				enb_pipo_data_x_l = 1'b1;
				enb_pipo_data_y_l = 1'b1;
				enb_pipo_result_l = 1'b0; 
				enb_pipo_reminder_l = 1'b0;
				enb_pipo_accumulator_rem_l = 1'b1;
				enb_operation_counter_l = 1'b1;
				sel_mux_prev_x_l = 1'b0;
				sel_mux_input_data_x_l = 1'b1;
				sel_mux_input_data_y_l = 1'b1;
				sel_mux_operation_res_l = 1'b0;
				sync_rst_l = 1'b0;
				ready_sig_l = 1'b0;
				load_x_sig_l = 1'b0;
				load_y_sig_l = 1'b0;
				enb_booth_l = 1'b1;
				flag_calculate_state_l = 1'b0;
				enb_pipo_x_mul_l = 1'b0;
				end
			SQRT_ROOT: begin
				enb_pipo_data_x_l = 1'b1;
				enb_pipo_data_y_l = 1'b1;
				enb_pipo_result_l = 1'b0; 
				enb_pipo_reminder_l = 1'b0;
				enb_pipo_accumulator_rem_l = 1'b1;
				enb_operation_counter_l = 1'b1;
				sel_mux_prev_x_l = 1'b0;
				sel_mux_input_data_x_l = 1'b1;
				sel_mux_input_data_y_l = 1'b0;
				sel_mux_operation_res_l = 1'b0;
				sync_rst_l = 1'b0;
				ready_sig_l = 1'b0;
				load_x_sig_l = 1'b0;
				load_y_sig_l = 1'b0;
				enb_booth_l = 1'b1;
				flag_calculate_state_l = 1'b0;
				enb_pipo_x_mul_l = 1'b0;
				end
			NOT_SUPPORTED: begin
				enb_pipo_data_x_l = 1'b1;
				enb_pipo_data_y_l = 1'b1;
				enb_pipo_result_l = 1'b0; 
				enb_pipo_reminder_l = 1'b0;
				enb_pipo_accumulator_rem_l = 1'b1;
				enb_operation_counter_l = 1'b1;
				sel_mux_prev_x_l = 1'b0;
				sel_mux_input_data_x_l = 1'b1;
				sel_mux_input_data_y_l = 1'b1;
				sel_mux_operation_res_l = 1'b0;
				sync_rst_l = 1'b0;
				ready_sig_l = 1'b0;
				load_x_sig_l = 1'b0;
				load_y_sig_l = 1'b0;
				enb_booth_l = 1'b1;
				flag_calculate_state_l = 1'b0;
				enb_pipo_x_mul_l = 1'b0;
				end
			default: begin
				enb_pipo_data_x_l = 1'b1;
				enb_pipo_data_y_l = 1'b1;
				enb_pipo_result_l = 1'b0; 
				enb_pipo_reminder_l = 1'b0;
				enb_pipo_accumulator_rem_l = 1'b1;
				enb_operation_counter_l = 1'b1;
				sel_mux_prev_x_l = 1'b0;
				sel_mux_input_data_x_l = 1'b1;
				sel_mux_input_data_y_l = 1'b0;
				sel_mux_operation_res_l = 1'b0;
				sync_rst_l = 1'b0;
				ready_sig_l = 1'b0;
				load_x_sig_l = 1'b0;
				load_y_sig_l = 1'b0;
				enb_booth_l = 1'b1;
				flag_calculate_state_l = 1'b0;
				enb_pipo_x_mul_l = 1'b0;
				end
			endcase
		end
	
	STATE_SET_RESULT: begin
			enb_pipo_data_x_l = 1'b0;
			enb_pipo_data_y_l = 1'b0;
			enb_pipo_result_l = 1'b1; 
			enb_pipo_reminder_l = 1'b1;
			enb_pipo_accumulator_rem_l = 1'b0;
			enb_operation_counter_l = 1'b1;
			sel_mux_prev_x_l = 1'b0;
			sel_mux_input_data_x_l = 1'b1;
			sel_mux_input_data_y_l = 1'b0;
			sel_mux_operation_res_l = (current_operation == SQRT_ROOT) ? 1'b0 : 1'b1;
			sync_rst_l = 1'b0;
			ready_sig_l = 1'b1;
			load_x_sig_l = 1'b0;
			load_y_sig_l = 1'b0;
			enb_booth_l = 1'b1;
			flag_calculate_state_l = 1'b1;
			enb_pipo_x_mul_l = 1'b0;
		end
	
	default: begin
			enb_pipo_data_x_l = 1'b0;
			enb_pipo_data_y_l = 1'b0;
			enb_pipo_result_l = 1'b1; 
			enb_pipo_reminder_l = 1'b1;
			enb_pipo_accumulator_rem_l = 1'b0;
			enb_operation_counter_l = 1'b1;
			sel_mux_prev_x_l = 1'b0;
			sel_mux_input_data_x_l = 1'b1;
			sel_mux_input_data_y_l = 1'b1;
			sel_mux_operation_res_l = 1'b0;
			sync_rst_l = 1'b0;
			ready_sig_l = 1'b0;
			load_x_sig_l = 1'b0;
			load_y_sig_l = 1'b0;
			enb_booth_l = 1'b0;
			flag_calculate_state_l = 1'b0;
			enb_pipo_x_mul_l = 1'b0;
		end
	endcase
	
end

// Output assignment section
assign enb_pipo_data_x = enb_pipo_data_x_l;
assign enb_pipo_data_y = enb_pipo_data_y_l;
assign enb_pipo_result = enb_pipo_result_l;
assign enb_pipo_reminder = enb_pipo_reminder_l;
assign enb_pipo_accumulator_rem = enb_pipo_accumulator_rem_l;
assign enb_operation_counter = enb_operation_counter_l;
assign sel_mux_input_data_x = sel_mux_input_data_x_l;
assign sel_mux_input_data_y = sel_mux_input_data_y_l;
assign sync_rst = sync_rst_l;
assign ready_signal = ready_sig_l;
assign load_x_signal = load_x_sig_l;
assign load_y_signal = load_y_sig_l;
assign sel_mux_prev_x = sel_mux_prev_x_l;
assign sel_mux_operation_res = sel_mux_operation_res_l;
assign enb_booth_o = enb_booth_l;
assign flag_calculate_state = flag_calculate_state_l;
assign enb_pipo_x_mult = enb_pipo_x_mul_l;

endmodule
