// Project: MDR
// Module:  Mux 3 to 1
// Author:  María Luisa Ortiz Carstensen
// Date:    April 14th, 2024

/*  This mux is the one that determines the following:  */
/*  00 -> Add zero
	 11 -> Add zero
	 10 -> Subtract M
	 01 -> Add M 													  */

import FSM_pkg::*;	 
	 
module mux_3to1
(	
  input   bit				Q,	// Selector [1], LSB of Q
  input   bit		 Qminus, // Selector [0]
  input	 [DW - 1: 0] M,		// Multiplicand
  
  output  [DW - 1 : 0] booth_op	// Output, M version
);

logic [DW - 1 : 0]op;

always_comb begin
		
    case ({Q,Qminus})
        2'b00: op = {DW {1'b0} }; // Add zero
        2'b11: op = {DW {1'b0} }; // Add zero
        2'b10: op = (~M) + 1'b1;  // Subtract M, A2 calculation is needed by the multiplication process
        2'b01: op = M;       	  // Add M
        default: op = {DW {1'b0} }; // Default to zero
    endcase
end

assign booth_op = op;
 
endmodule    
	 


						  
						  