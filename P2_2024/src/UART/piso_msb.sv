// Project: UART ECHO
// Module:  UART Tx - PISO MSB
// Author:  Víctor Cervantes Gómez
// Date:    March 30th, 2024


module piso_msb #(
//parameter type DATA_T = regs_pkg::piso_data_t // Syntax not supported by Quartus
parameter W_PISO_MSB = 8
) (
input bit                     clk,    // Clock
input bit                     rst,    // asynchronous reset low active 
input logic                   enb,    // Enable
input logic                   l_s,    // load or shift
input logic [W_PISO_MSB-1:0]  inp,    // data input
   output logic                  out     // Serial output
);

typedef logic [W_PISO_MSB-1:0]   piso_data_t;

piso_data_t     rgstr_r     ;
piso_data_t     rgstr_nxt   ;

// Combinational module
always_comb begin
    if (l_s)
        rgstr_nxt  = inp;
    else
        rgstr_nxt  = {rgstr_r[W_PISO_MSB-2:0], rgstr_r[W_PISO_MSB-1]};
   //      rgstr_nxt  = rgstr_r<<1;
end

always_ff@(posedge clk or negedge rst) begin: rgstr_label
       if(!rst)
        rgstr_r     <= '0           ;
    else if (enb) begin
        rgstr_r     <= rgstr_nxt    ;
    end
end:rgstr_label

assign out  = rgstr_r[W_PISO_MSB-1];    // MSB bit is the first to leave

endmodule

