// Project: UART ECHO
// Module:  UART_ECHO
// Author:  María Luisa Ortiz Carstensen
// Date:    March 29th, 2024

module UART_ECHO 
#(
	parameter DW = 8
	//parameter PARITY = 2'b01, //2'b0 = none, 2'b01 = odd, 2'b10 = even, 2'b11 = not used
   //parameter SIMULATION_ECHO = 1'b1, //1'b0 = 10 MHz (echo), 1'b1 = 50 MHz (simulation)
	//parameter BAUDRATE = 19200
  )
(
	input clk_50MHz,
	input rst,
	input rx,		// Rx Data
	input enb,
//	input RTS,		// Request to Send
	
	output tx,	// Tx Data 
	output tx_ready
//	output CTS // Clear to Send
);

// WIRES
wire clk_w;
wire clk_10Mhz_w;


// Input Wires
wire rx_i_w;
wire syncrst_w;
wire rx_ready_tx_start_w;
wire [DW-1:0]rx_to_tx_w;

// Output Wires
wire tx_ready_w;
wire tx_frame_w;


// Clock Instance
PLL_MDR pll
(
	.areset(~rst),
	.inclk0(clk_50MHz),
	.c0(clk_10Mhz_w),
	.locked(/* Blank */)
);

// Tx Instance
uart_tx #(.DW(DW)) Tx_Top 
(
    .rst(rst),
    .sync_rst(0), // syncrst_w
    .clk(clk_10Mhz_w), // Use the correct clock signal
    .start_tx(rx_ready_tx_start_w),
    .even_odd(0), // 0 for even, 1 for odd
    .parity(1),   // Parity active
    .data_to_send(rx_to_tx_w),
	 
    .frame(tx_frame_w),
    .ready(tx_ready_w)
);

// Rx Instance
UART_Rx #(.DW(DW)) Rx_Top
(
	.clk(clk_10Mhz_w),
	.enb(enb),
	.rx_i(rx),
	.rst(rst),
	
	.rx_o(rx_to_tx_w),
	.rx_ready(rx_ready_tx_start_w)
);

assign tx = tx_frame_w;
assign tx_ready = tx_ready_w;
//assign clk_10Mhz_w = clk_50MHz; 

endmodule 