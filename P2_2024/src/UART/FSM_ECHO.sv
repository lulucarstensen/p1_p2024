// Project: UART ECHO
// Module:  FSM USART ECHO
// Author:  María Luisa Ortiz Carstensen
// Date:    March 31st, 2024

module FSM_ECHO
(  
    input clk,
    input rst,
    input enb,
	 input receiving,
	 input transmitting,
	 
	 output send,
	 output syncrst
);

typedef enum logic[1:0]{
	IDLE		= 2'b00,
	RECEIVE	= 2'b01,
	TRANSMIT	= 2'b10,
	SEND		= 2'b11
} fsm_type;

fsm_type current_state;
fsm_type next_state;

logic send_l;
logic syncrst_l;

always_ff @(posedge clk, negedge rst) begin

	if(!rst)
		current_state <= IDLE;
	else
		current_state <= next_state;
		
end

// State Transitions 
// Wnen 0, it's taken
// When 1, it's done
always_comb begin

		case(current_state)
		
			IDLE:
				begin
					next_state = (receiving == 1'b0) ? RECEIVE : IDLE;
				end
				
			RECEIVE:
				begin
					next_state = (receiving == 1'b1) ? TRANSMIT : RECEIVE;
				end
				
			TRANSMIT: 
				begin
					next_state = SEND;//transmitting = 1? 
				end
				
			SEND:
				begin
					next_state = (transmitting == 1'b0) ? SEND : IDLE;
				end
				
			default:
				begin
					next_state = IDLE;
				end
				
		endcase
end


// State Outputs
always_comb begin 

	case(current_state)
	
			IDLE:
				begin
					send_l		= 1'b1;
					syncrst_l	= 1'b0; 
				end
				
			RECEIVE:
				begin
					send_l		= 1'b1;
					syncrst_l	= 1'b0;
				end
				
			TRANSMIT:
				begin
					send_l		= 1'b0;
					syncrst_l	= 1'b1;
				end
				
			SEND:
				begin
					send_l		= 1'b1;
					syncrst_l	= 1'b0;
				end
				
			default:
				begin
					send_l		= 1'b1;
					syncrst_l	= 1'b0;
				end
		endcase
end

assign send		= send_l;
assign syncrst = syncrst_l;

endmodule
