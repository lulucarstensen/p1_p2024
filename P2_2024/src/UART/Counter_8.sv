// Project: UART ECHO
// Module:  UART Rx - Counter 8
// Author:  María Luisa Ortiz Carstensen
// Date:    March 30th, 2024


module Counter_8 #(parameter DW = 4, parameter OV = 9, parameter COUNT = 520, parameter COUNTDW = $clog2(COUNT))
(
	input 	clk, 
	input 	enb,
	input		rst,
	input 	sync_rst,
	input		[COUNTDW-1:0]count5_i,
	
	output			[DW-1:0]count8_o,
	output logic	ovf8,
	output logic	parity_o
);

logic [DW-1:0] count8_l;

// Overflow logic
always_comb begin
  if (count8_o == OV) begin
		ovf8 = 1'b1;// Set overflow
  end 
  else begin
		ovf8 = 1'b0;// Clear overflow
  end
  if (count8_o == 10) begin
		parity_o = 1'b1;
  end
  else begin
		parity_o = 1'b0;// Clear parity
  end
end


always_ff @(posedge clk or negedge rst) begin
  if (!rst) begin
		count8_l <= 0;//active on 0
  end
  else begin
		if (sync_rst)
			 count8_l <= 0;
		else if (enb) begin
			 if (count5_i == (COUNT - 1))
				  count8_l <= count8_l + 1;
			
			 if (count8_l == 11) begin //The 11th count is the stop bit
				  count8_l <= 0;
			 end
		end
  end
end

assign count8_o = count8_l;

endmodule 