// Project: UART ECHO
// Module:  UART Tx - Frame Creator
// Author:  Víctor Cervantes Gómez
// Date:    March 30th, 2024
module frame_creator
(
	input clk,
	input reset, 
	input sync_rst,
	input parity_in,
	input [8:0]data_in,  
	output [11:0]data_out
);

logic [11:0] parity_frame;

always_comb begin
	case(parity_in)
		1'b0: begin
				parity_frame = {1'b0,data_in[8:1],3'b111};
		end
		1'b1: begin
				parity_frame = {1'b0,data_in,2'b11};
		end
		default: begin
			parity_frame = {1'b0,data_in,2'b11};
			end
	endcase
end

assign data_out = parity_frame;

endmodule
