// Project: UART ECHO
// Module:  UART Tx - TOP
// Author:  Víctor Cervantes Gómez
// Date:    March 30th, 2024


// UART transmiter module
// frec 10 Millones de dato por segndo, 
// si tienes br de 19200, envias 19200 datos cada segundo, 
// para ajustar divides 10M sobre 19200
// te da frec reloj que tiene que tener tx 


module uart_tx 
#(
	parameter DW = 8,
   parameter COUNT = 520,
	parameter COUNTDW = $clog2(COUNT)
)
(
input rst,
input sync_rst,
input clk,
input start_tx,
input even_odd,
input parity,
input [DW - 1 : 0] data_to_send,
output frame,
output ready
);

logic sync_rst_l;
logic [8 : 0] data_with_parity_l;
logic [11 : 0] frame_to_send;
logic [COUNTDW-1 : 0] counter_5_l;
logic overflow_c5_l;
logic [3 : 0] counter_11_l;
logic overflow_c11_l;
logic load_shitf_l;
logic serial_data_l;
logic mux_sel;
logic frame_to_end_l;
logic en_piso_l;
logic en_count_ov_l;
logic ready_l;
logic en_cnt5_l;

parity_mod parity_frame
(
	.clk(clk), 
	.reset(rst), 
	.sync_rst(sync_rst_l),
	.even_odd_i(even_odd),
	.data_in(data_to_send),  
	.data_out(data_with_parity_l)
);

frame_creator frame_mod
(
	.clk(clk), 
	.reset(rst), 
	.sync_rst(sync_rst_l),
	.parity_in(parity),
	.data_in(data_with_parity_l),  
	.data_out(frame_to_send)
);

Counter
#(
   .DW(COUNTDW),
	.OV(COUNT-1)
)
counter_5
(
   .enable_i(en_cnt5_l),
   .clk_i(clk),
   .rst_i(rst),
   .sync_rst(sync_rst_l),
   .Output_o(counter_5_l),
	.ovflw(overflow_c5_l)
);

Counter
#(
   .DW(4),
	.OV(11)
)
counter_ov
(
   .enable_i(en_count_ov_l),
   .clk_i(clk),
   .rst_i(rst),
   .sync_rst(sync_rst_l),
   .Output_o(counter_11_l),
	.ovflw(overflow_c11_l)
);

piso_msb
#(
	.W_PISO_MSB(12)
)
piso
(
.clk(clk),    // Clock
.rst(rst),    // asynchronous reset low active 
.enb(en_piso_l),    // Enable
.l_s(load_shitf_l),    // load or shift
.inp(frame_to_send),    // data input
.out(serial_data_l)     // Serial output
);

mux_DW
#(
    .DW(1)  // Data width
)
mux_piso
(
	 .sel(mux_sel),         // Signal bit selection (N bits)
	 .dataA(serial_data_l),  				     // Multiplier Input (N x DATA_WIDTH bits)
	 .dataB(1'b1),// Multiplier shifted as input
	 .dataOut (frame_to_end_l)       // Output from mux
);


FSM_Machine
uart_fsm
(
	.clk(clk),
	.reset(rst),
	.ov_cntov(overflow_c11_l),
	.start_tx(start_tx),
	.ov_cnt5(overflow_c5_l),
	.syncreset_i(sync_rst),
	.ov_count(counter_11_l),
	
	.load_or_shift(load_shitf_l),
	.en_piso(en_piso_l),
	.en_cnt_ov(en_count_ov_l),
	.en_cnt5(en_cnt5_l),
	.syncreset_o(sync_rst_l),
	.ready_o(ready_l),
	.mux_sel_o(mux_sel)
);

assign frame = frame_to_end_l;
assign ready = ready_l;


endmodule
