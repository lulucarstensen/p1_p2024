// Author: Maria Luisa Ortiz Carstensen
// Project: MDR
// Module: Interface for Top Module
// Date: 13/02/2024

localparam DW = 10;
localparam SEL = 1;

interface MDR_Interface;
    // Define input ports
    logic start; // Start signal
    logic load; // Load signal
    logic [DW-1:0] data_in; // Data input
    logic [SEL:0] operation_sel; // Operation selection

    // Define output ports
    logic [DW-1:0] result; // Result output
    logic [DW-1:0] remainder; // Remainder output
    logic ready; // Ready signal
	 logic load_X;
	 logic load_Y;
	 logic error;

    modport tb_mdr 
	 (
    input start,
    input load,
    input data_in,
    input operation_sel,
    output result,
    output remainder,
    output ready,
    output load_X,
    output load_Y,
    output error
    );
endinterface
