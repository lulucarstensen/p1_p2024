//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		Sequential Multiplier
//Module:  		Mux Data Width
//Date:	  		13/02/2024

module mux_DW #(parameter DW = 8)
(
  input   bit  		  		  sel,	// Mux Selector
  input   [DW - 1 : 0]		dataA,	// Input (N x DW bits)
  input   [DW - 1 : 0]		dataB,
  
  output  [DW - 1 : 0]	  dataOut	//Output (Data Width)
);
	
 assign dataOut = (sel == 0) ? dataA : dataB;
 
endmodule