//Author:  		Maria Luisa Ortiz Carstensen
//Project: 		MDR
//Module:  		Mux 2 to 1 for product
//Date:	  		13/02/2024

import FSM_pkg::*;

module mux_product
(
  input   bit  		  		  sel,	// Shift ovf
  input   [(DW*2) : 0]		dataA,	// Zero 
  input   [(DW*2) : 0]		dataB,   // Shifted value
  
  output  [(DW*2) : 0]	  product	// Output (Data Width)
);
	
 assign product = (sel == 0) ? dataA : dataB;
 
endmodule