`timescale 1ns / 1ps

module tbp2_intf;

    // Declare testbench interface instance
    MDR_Interface tb_intf();

    // Declare clock and reset signals
    logic clk_50MHz;
    logic rst;

    // Instantiate sqrt_root module using the testbench interface
    sqrt_root uut
    (
        .clk_50MHz(clk_50MHz),
        .rst(rst),
        .start(tb_intf.start),
        .load(tb_intf.load),
        .data_in(tb_intf.data_in),
        .operation_sel(tb_intf.operation_sel),

        .result(tb_intf.result),
        .remainder(tb_intf.remainder),
        .ready(tb_intf.ready),
        .load_X(tb_intf.load_X),
        .load_Y(tb_intf.load_Y),
        .error(tb_intf.error)
    );

    // Initial block to initialize signals
    initial begin
        // Initialize clock and reset
        clk_50MHz = 0;
        rst = 1;

        // Wait for a few cycles
        #5;

        // Deassert reset
        rst = 0;

        // Wait for a few cycles
        #5;

        // Start the operation
        tb_intf.start = 1;

        // Load data_in
        tb_intf.load = 1;
        tb_intf.data_in = 7;
        #5;
        tb_intf.data_in = 10;
        #5;

        // Stop simulation
        $stop;
    end

    // Clock generation
    always #1 clk_50MHz = ~clk_50MHz;

endmodule
