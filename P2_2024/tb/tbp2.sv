`timescale 1ns / 1ps

localparam DW = 10;
localparam OP = 1;

module tbp2;

logic  		clk_50MHz;
logic  		rst;
logic 		start;
logic 		load;
logic [DW :0] data_in;
logic [OP:0] operation_sel;

logic [DW :0] result;
logic [DW :0] remainder;
logic  		ready;
logic  		load_X;
logic  		load_Y;
logic  		error;

sqrt_root uut
(
.clk_50MHz(clk_50MHz),
.rst(rst),
.start(start),
.load(load),
.data_in(data_in),
.operation_sel(operation_sel),

.result(result),
.remainder(remainder),
.ready(ready),
.load_X(load_X),
.load_Y(load_Y),
.error(error)
);


initial begin
		#2	clk_50MHz     = 0;
		#2	rst     = 0;
		#2	start  = 0;
		#2	data_in = 10'b0000000000;
		#2	load = 1'b0;
		#2	operation_sel = 2'b00;
			

		#2	rst = 0;
		#2	rst = 1;
		#2 	start  = 1'b1;
			operation_sel = 2'b00;
		#2 	load = 1'b1;
			data_in = 7;
			start  = 1'b0;
		#2 	load = 1'b1;
			data_in = 10;

			#30 $stop;
end

always begin
    #1 clk_50MHz <= ~clk_50MHz;
end

endmodule
